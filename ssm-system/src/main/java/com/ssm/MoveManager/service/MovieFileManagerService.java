package com.ssm.MoveManager.service;

import com.ssm.common.util.layui.LayuiUnit;
import com.ssm.mapper.system.movemanager.MoveFileMapper;
import com.ssm.po.system.movemanager.MovieFilePo;
import com.ssm.vo.system.movemanager.MoveFileQueryVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class MovieFileManagerService {
    @Resource
    private MoveFileMapper moveFileMapper;

    /**
     * 获取查询结果
     *
     * @param moveFileQueryVo
     * @return
     */
    public LayuiUnit queryMoveFile(MoveFileQueryVo moveFileQueryVo) {
        String name = moveFileQueryVo.getFileName();
        if (StringUtils.isNotEmpty(name)) {
            moveFileQueryVo.setFileName("%" + name + "%");
        }

        Integer page = moveFileQueryVo.getPage();
        Integer limit = moveFileQueryVo.getLimit();
        moveFileQueryVo.setBegincount((page - 1) * limit + 1);
        moveFileQueryVo.setEndcount(page * limit);

        return LayuiUnit.data(moveFileMapper.getMovieFileCount(moveFileQueryVo)
                , moveFileMapper.queryMovieFile(moveFileQueryVo));
    }

    /**
     * 递归扫描指定文件夹下面的指定文件
     */
    public void scanFilesWithRecursion(String folderPath) {
        File directory = new File(folderPath);
        if (directory.isDirectory()) {
            File[] filelist = directory.listFiles();
            for (int i = 0; i < filelist.length; i++) {
                /**如果当前是文件夹，进入递归扫描文件夹**/
                if (filelist[i].isDirectory()) {
                    /**递归扫描下面的文件夹**/
                    scanFilesWithRecursion(filelist[i].getAbsolutePath());
                } else {
                    File file = filelist[i];
                    String fileName = file.getName();
                    if (!fileName.contains(".jpg") && !fileName.contains(".torrent") && !fileName.contains(".JPG")
                            && !fileName.contains(".jpeg") && !fileName.contains(".txt")&& !fileName.contains(".url")) {
                        MovieFilePo moveFilePo = new MovieFilePo();
                        moveFilePo.setFileName(file.getName());
                        moveFilePo.setFilePath(file.getAbsolutePath());
                        moveFileMapper.insert(moveFilePo);
                    }
                }
            }
        }
    }

    /**
     * 处理文件名重复
     */
    public void dealRepeatMoveFile() {
        List<MovieFilePo> fileList = moveFileMapper.getAllMovieFile();
        fileList.stream().forEach(movieFilePo -> {
            // 去除中文名与日文名
            String deleteModel = "[\\u4e00-\\u9fa5]*[\\u0800-\\u4e00]*";
            Pattern deletePattern = Pattern.compile(deleteModel);
            Matcher deleteMatcher = deletePattern.matcher(movieFilePo.getFileName());
            String deleteFileName = deleteMatcher.replaceAll("");
            // 过滤车牌
            String checkFileModel = "[a-zA-Z-0-9_]*";
            Pattern checkPattern = Pattern.compile(checkFileModel);
            Matcher CheckMatcher = checkPattern.matcher(deleteFileName);
            if (CheckMatcher.find()) {
                String importFileName1 = CheckMatcher.group(0);
                if (importFileName1.length() >= 5) {
                    updateSetMovieRepead(movieFilePo, importFileName1.toLowerCase());
                    updateSetMovieRepead(movieFilePo, importFileName1.toUpperCase());
                }
            }
        });
    }

    private void updateSetMovieRepead(MovieFilePo movieFilePo,String importFileName1){
        List<MovieFilePo> repeatList = moveFileMapper.checkRepeatFile(movieFilePo.getId(), "%" + importFileName1 + "%");
        repeatList.stream().forEach(repeatMovieFilePo -> {
            moveFileMapper.updateSetMovieRepead(repeatMovieFilePo.getId());
        });
        if (repeatList.size() > 0) {
            System.out.println(importFileName1);
            moveFileMapper.updateSetMovieRepead(movieFilePo.getId());
        }
    }

}
