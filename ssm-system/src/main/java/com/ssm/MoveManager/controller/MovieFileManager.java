package com.ssm.MoveManager.controller;

import com.ssm.MoveManager.service.MovieFileManagerService;
import com.ssm.common.util.layui.LayuiUnit;
import com.ssm.common.util.system.ISsmResponse;
import com.ssm.vo.system.movemanager.MoveFileQueryVo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * 流程定义管理
 *
 * @author yuze
 */
@RestController
@RequestMapping("/moveFileManager")
public class MovieFileManager {
    @Resource
    private MovieFileManagerService movieFileManagerService;
    /**
     * 电影文件管理主页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/moveFileManagerView")
    public ModelAndView MoveFileManagerView() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("/system/movemanager/MoveFileManager_Mian");
        return modelAndView;
    }

    /**
     * 电影文件查询
     *
     */
    @RequestMapping("/queryMoveFile")
    public @ResponseBody
    LayuiUnit queryMoveFile(MoveFileQueryVo moveFileQueryVo) {
        return movieFileManagerService.queryMoveFile(moveFileQueryVo);
    }

    /**
     * 电影文件名扫描
     *
     */
    @RequestMapping(value = "/insertMoveFile", method = {RequestMethod.POST})
    public @ResponseBody ISsmResponse insertMoveFile(String filePath) {
        ISsmResponse res = new ISsmResponse();
        movieFileManagerService.scanFilesWithRecursion(filePath);
        res.setMessage(res, "成功");
        return res;
    }

    /**
     * 处理名称重复文件
     */
    @RequestMapping(value = "/dealRepeatMoveFile", method = {RequestMethod.GET})
    public @ResponseBody ISsmResponse dealRepeatMoveFile() {
        ISsmResponse res = new ISsmResponse();
        movieFileManagerService.dealRepeatMoveFile();
        res.setMessage(res, "成功");
        return res;
    }
}
