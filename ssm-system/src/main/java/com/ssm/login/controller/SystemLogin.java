package com.ssm.login.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 登录相关
 *
 * @author yuze
 */
@RestController
@RequestMapping("/system")
public class SystemLogin {
    /**
     * 系统登录页面
     *
     * @return loginView
     */
    @RequestMapping("/login_view")
    public ModelAndView loginView() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("/login/login");
        return modelAndView;
    }
}
