package com.ssm.login.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/system")
public class SystemPage {
    @RequestMapping("/systemMain")
    public ModelAndView systemMain() throws Exception {

        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("system");

        return modelAndView;

    }
}
