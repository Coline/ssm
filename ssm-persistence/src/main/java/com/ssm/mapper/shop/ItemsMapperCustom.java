package com.ssm.mapper.shop;

import com.ssm.po.shop.Items;
import com.ssm.vo.shop.ItemsCustom;
import com.ssm.vo.shop.ItemsQueryVo;

import java.util.List;

public interface ItemsMapperCustom {
    /**
     * 查询商品
     * @param itemsQueryVo:查询条件
     * @return List<ItemsCustom>
     * @throws Exception：Exception异常
     */
    List<ItemsCustom> findItemsList(ItemsQueryVo itemsQueryVo)throws Exception;
    /**
     * 查询商品总数
     * @param itemsQueryVo:查询条件
     * @return Integer
     * @throws Exception：Exception异常
     */
    Integer findItemsCount(ItemsQueryVo itemsQueryVo)throws Exception;
    /**
     * 添加商品
     * @param items:商品PO
     * @throws Exception：Exception异常
     */
    void insertItems(Items items)throws Exception;
    /**
     * 删除商品
     * @param id:商品id
     * @throws Exception：Exception异常
     */
    void deleteItems(String id)throws Exception;
    /**
     * 批量删除商品
     * @param ids:商品id逗号拼接字符串
     * @throws Exception：Exception异常
     */
    void deleteCheckedItems(String ids)throws Exception;
    /**
     * 获取商品信息
     * @param id:商品id
     * @throws Exception：Exception异常
     */
    Items getItems(String id)throws Exception;
    /**
     * 修改商品信息
     * @param items:商品PO
     * @throws Exception：Exception异常
     */
    void updateItems(Items items)throws Exception;
}