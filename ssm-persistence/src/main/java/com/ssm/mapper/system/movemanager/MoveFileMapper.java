package com.ssm.mapper.system.movemanager;

import com.ssm.po.system.movemanager.MovieFilePo;
import com.ssm.vo.system.movemanager.MoveFileQueryVo;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface MoveFileMapper {

    Integer getMovieFileCount(MoveFileQueryVo moveFileQueryo);

    List<MovieFilePo> queryMovieFile(MoveFileQueryVo moveFileQueryVo);

    @Insert("insert into movie_file(ID,FILE_NAME,FILE_PATH,CREATE_DATE,IS_REPEAT)"
            +" values(MOVIE_FILE_SEQ.nextval,#{fileName},#{filePath},sysdate,'0')")
    void insert(MovieFilePo movieFilePo);

    @Select("select ID,FILE_NAME,FILE_PATH,CREATE_DATE,IS_REPEAT from movie_file")
    @ResultMap("BaseResultMap")
    List<MovieFilePo> getAllMovieFile();

    @Select("select ID,FILE_NAME,FILE_PATH,CREATE_DATE,IS_REPEAT from movie_file where ID <> #{id} and file_name like #{fileName}")
    @ResultMap("BaseResultMap")
    List<MovieFilePo> checkRepeatFile(@Param("id") String id,@Param("fileName") String fileName);

    @Update("update movie_file set IS_REPEAT = '1' where id = #{id}")
    void updateSetMovieRepead(@Param("id") String id);
}
