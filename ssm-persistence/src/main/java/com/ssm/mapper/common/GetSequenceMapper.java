package com.ssm.mapper.common;

public interface GetSequenceMapper {
    /**
     * 获取sequence序列值
     * @param sequence:sequence名称
     * @return String
     * @throws Exception:Exception异常
     */
    String getSequence(String sequence)throws Exception;
}