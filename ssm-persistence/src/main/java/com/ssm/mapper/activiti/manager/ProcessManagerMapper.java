package com.ssm.mapper.activiti.manager;

import com.ssm.po.activiti.manager.ActReProcdef;
import com.ssm.vo.activiti.manager.BaseManagerQueryVo;

import java.util.List;

public interface ProcessManagerMapper {

    List<ActReProcdef> getProcessDefineList(BaseManagerQueryVo baseManagerQueryVo);

    Integer getProcessDefineCount(BaseManagerQueryVo baseManagerQueryVo);


}
