package com.ssm.mapper.activiti.manager;

import com.ssm.po.activiti.manager.ActReModel;
import com.ssm.vo.activiti.manager.BaseManagerQueryVo;

import java.util.List;

public interface ModelManagerMapper {

    List<ActReModel> findModelList(BaseManagerQueryVo modelManagerQueryVo);

    Integer getModelCount(BaseManagerQueryVo modelManagerQueryVo);

}
