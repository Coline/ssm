package com.ssm.mapper.cloth.label;

import com.ssm.po.cloth.label.ClothLabelPo;
import com.ssm.vo.cloth.label.ClothLabelQueryVo;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ClothLabelMapper {

    Integer getClothLabelCount(ClothLabelQueryVo clothLabelQueryVo);

    List<ClothLabelPo> queryClothLabel(ClothLabelQueryVo clothLabelQueryVo);

    @Insert("INSERT INTO CLOTH_LABEL(ID,SERIAL_NUMBER,COMPONENT,WARP,WEFT,WARP_INTENSITY" +
            ",WEFT_INTENSITY,WIDTH,GRAM_WEIGHT,POST_FINISHING,SUPPLIER_FABRIC_NUMBER,SUPPLIER_NAME" +
            ",SUPPLIER_QUOTATION,CREATE_DATE,CREATE_USER,VIEW_TOTAL)"
            +" values(CLOTH_LABEL_SEQ.nextval,#{serialNumber},#{component},#{warp},#{weft},#{warpIntensity}" +
            ",#{weftIntensity},#{width},#{gramWeight},#{postFinishing},#{supplierFabricNumber},#{supplierName}" +
            ",#{supplierQuotation},sysdate,'',#{viewTotal})")
    void insert(ClothLabelPo clothLabelPo);

    @Select("select ID,SERIAL_NUMBER,COMPONENT,WARP,WEFT,WARP_INTENSITY,WEFT_INTENSITY,WIDTH" +
            ",GRAM_WEIGHT,POST_FINISHING,SUPPLIER_FABRIC_NUMBER,SUPPLIER_NAME,SUPPLIER_QUOTATION" +
            ",CREATE_DATE,MODIFY_DATE,CREATE_USER,VIEW_TOTAL from CLOTH_LABEL where id = #{id}")
    @ResultMap("BaseResultMap")
    ClothLabelPo getClothLabelByParamKey(@Param("id") String id);

    @Update({
            "update CLOTH_LABEL set SERIAL_NUMBER = #{serialNumber}"
             ,",COMPONENT = #{component},WARP = #{warp},WEFT = #{weft},WARP_INTENSITY = #{warpIntensity}"
             ,",WEFT_INTENSITY = #{weftIntensity},WIDTH = #{width},GRAM_WEIGHT = #{gramWeight}"
             ,",POST_FINISHING = #{postFinishing},SUPPLIER_FABRIC_NUMBER = #{supplierFabricNumber}"
             ,",SUPPLIER_NAME = #{supplierName},SUPPLIER_QUOTATION = #{supplierQuotation}"
             ,",MODIFY_DATE = sysdate,VIEW_TOTAL = #{viewTotal} where ID = #{id}"
    })
    void updateLabel(ClothLabelPo clothLabelPo);

    @Delete({"delete from CLOTH_LABEL where instr(#{ids},','||id||',')>0"})
    void deleteLabel(@Param("ids") String ids);

}
