package com.ssm.mapper.cloth.label;

import com.ssm.po.cloth.label.LabelPicturePo;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface LabelPictureMapper {

    @Insert({"INSERT INTO LABEL_PICTURE(ID,LABEL_ID,PICTURE_NAME,PICTURE_SRC,SORT)"
            ," values(LABEL_PICTURE_SEQ.nextval,#{labelId},#{pictureName},#{pictureSrc},1)"})
    void insert(LabelPicturePo labelPicturePo);


    @Select({
            "SELECT picture_src FROM label_picture where label_id = #{labelId} order by sort desc"
    })
    @ResultMap("BaseResultMap")
    List<LabelPicturePo> getLabelPicturePictureByLabelId(String labelId);

    @Select({
            "SELECT picture_src FROM label_picture where instr(#{labelIds},','||label_id||',')>0 order by sort desc"
    })
    @ResultMap("BaseResultMap")
    List<LabelPicturePo> getLabelPicturePictureByLabelIds(@Param("labelIds") String labelIds);


    @Delete({
            "delete from label_picture where instr(#{labelIds},','||label_id||',')>0"
    })
    int deletePictureByLabelIds(@Param("labelIds") String labelIds);
}
