package com.ssm.vo.shop;

/**
 * 
 * <p>Title: ItemsQueryVo</p>
 * <p>Description:商品包装对象 </p>
 */
public class ItemsQueryVo {

	private String name;
	private String price;
	private String detail;
	//页码
	private Integer page;
	//数量
	private Integer limit;

	//页码
	private Integer begincount;
	//数量
	private Integer endcount;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getBegincount() {
		return begincount;
	}

	public void setBegincount(Integer begincount) {
		this.begincount = begincount;
	}

	public Integer getEndcount() {
		return endcount;
	}

	public void setEndcount(Integer endcount) {
		this.endcount = endcount;
	}
}
