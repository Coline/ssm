package com.ssm.vo.cloth.label;

public class ClothLabelQueryVo {
    private String serialNumber;

    //页码
    private Integer page;
    //数量
    private Integer limit;

    //页码
    private Integer begincount;
    //数量
    private Integer endcount;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getBegincount() {
        return begincount;
    }

    public void setBegincount(Integer begincount) {
        this.begincount = begincount;
    }

    public Integer getEndcount() {
        return endcount;
    }

    public void setEndcount(Integer endcount) {
        this.endcount = endcount;
    }

}
