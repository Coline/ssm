package com.ssm.vo.system.movemanager;

public class MoveFileQueryVo {
    private String fileName;
    private String isRepeat;

    //页码
    private Integer page;
    //数量
    private Integer limit;

    //页码
    private Integer begincount;
    //数量
    private Integer endcount;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getBegincount() {
        return begincount;
    }

    public void setBegincount(Integer begincount) {
        this.begincount = begincount;
    }

    public Integer getEndcount() {
        return endcount;
    }

    public void setEndcount(Integer endcount) {
        this.endcount = endcount;
    }

    public String getIsRepeat() {
        return isRepeat;
    }

    public void setIsRepeat(String isRepeat) {
        this.isRepeat = isRepeat;
    }
}
