package com.ssm.vo.activiti.manager;

public class BaseManagerQueryVo {
    private String name;
    private String key;
    //页码
    private Integer page;
    //数量
    private Integer limit;

    //页码
    private Integer begincount;
    //数量
    private Integer endcount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getBegincount() {
        return begincount;
    }

    public void setBegincount(Integer begincount) {
        this.begincount = begincount;
    }

    public Integer getEndcount() {
        return endcount;
    }

    public void setEndcount(Integer endcount) {
        this.endcount = endcount;
    }
}
