package com.ssm.vo.activiti.manager;

public class ProcessDefineModel extends BaseModelAddVo {
    private String deploymentId;

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }
}
