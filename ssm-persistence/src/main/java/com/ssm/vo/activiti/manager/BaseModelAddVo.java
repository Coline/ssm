package com.ssm.vo.activiti.manager;

public class BaseModelAddVo {
    public String name;
    public String description;
    public String key;
    public String initBytesJson;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getInitBytesJson() {
        return initBytesJson;
    }

    public void setInitBytesJson(String initBytesJson) {
        this.initBytesJson = initBytesJson;
    }
}
