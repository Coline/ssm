package com.ssm.po.cloth.label;

import java.util.Date;

public class ClothLabelPo {
    private String id;
    private String serialNumber;
    private String component;
    private String warp;
    private String weft;
    private String warpIntensity;
    private String weftIntensity;
    private String width;
    private String gramWeight;
    private String postFinishing;
    private String supplierFabricNumber;
    private String supplierName;
    private String supplierQuotation;
    private Date createDate;
    private Date modifyDate;
    private String createUser;
    private String viewTotal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getWarp() {
        return warp;
    }

    public void setWarp(String warp) {
        this.warp = warp;
    }

    public String getWeft() {
        return weft;
    }

    public void setWeft(String weft) {
        this.weft = weft;
    }

    public String getWarpIntensity() {
        return warpIntensity;
    }

    public void setWarpIntensity(String warpIntensity) {
        this.warpIntensity = warpIntensity;
    }

    public String getWeftIntensity() {
        return weftIntensity;
    }

    public void setWeftIntensity(String weftIntensity) {
        this.weftIntensity = weftIntensity;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getGramWeight() {
        return gramWeight;
    }

    public void setGramWeight(String gramWeight) {
        this.gramWeight = gramWeight;
    }

    public String getPostFinishing() {
        return postFinishing;
    }

    public void setPostFinishing(String postFinishing) {
        this.postFinishing = postFinishing;
    }

    public String getSupplierFabricNumber() {
        return supplierFabricNumber;
    }

    public void setSupplierFabricNumber(String supplierFabricNumber) {
        this.supplierFabricNumber = supplierFabricNumber;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierQuotation() {
        return supplierQuotation;
    }

    public void setSupplierQuotation(String supplierQuotation) {
        this.supplierQuotation = supplierQuotation;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getViewTotal() {
        return viewTotal;
    }

    public void setViewTotal(String viewTotal) {
        this.viewTotal = viewTotal;
    }
}
