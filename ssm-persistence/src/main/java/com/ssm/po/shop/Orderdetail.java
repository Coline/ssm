package com.ssm.po.shop;

public class Orderdetail {
    private String id;

    private String ordersid;

    private String itemsid;

    private String itemsnum;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getOrdersid() {
        return ordersid;
    }

    public void setOrdersid(String ordersid) {
        this.ordersid = ordersid == null ? null : ordersid.trim();
    }

    public String getItemsid() {
        return itemsid;
    }

    public void setItemsid(String itemsid) {
        this.itemsid = itemsid == null ? null : itemsid.trim();
    }

    public String getItemsnum() {
        return itemsnum;
    }

    public void setItemsnum(String itemsnum) {
        this.itemsnum = itemsnum == null ? null : itemsnum.trim();
    }
}