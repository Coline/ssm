package com.ssm.po.shop;

import java.util.ArrayList;
import java.util.List;

public class OrderdetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrderdetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOrdersidIsNull() {
            addCriterion("ORDERSID is null");
            return (Criteria) this;
        }

        public Criteria andOrdersidIsNotNull() {
            addCriterion("ORDERSID is not null");
            return (Criteria) this;
        }

        public Criteria andOrdersidEqualTo(String value) {
            addCriterion("ORDERSID =", value, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidNotEqualTo(String value) {
            addCriterion("ORDERSID <>", value, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidGreaterThan(String value) {
            addCriterion("ORDERSID >", value, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidGreaterThanOrEqualTo(String value) {
            addCriterion("ORDERSID >=", value, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidLessThan(String value) {
            addCriterion("ORDERSID <", value, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidLessThanOrEqualTo(String value) {
            addCriterion("ORDERSID <=", value, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidLike(String value) {
            addCriterion("ORDERSID like", value, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidNotLike(String value) {
            addCriterion("ORDERSID not like", value, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidIn(List<String> values) {
            addCriterion("ORDERSID in", values, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidNotIn(List<String> values) {
            addCriterion("ORDERSID not in", values, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidBetween(String value1, String value2) {
            addCriterion("ORDERSID between", value1, value2, "ordersid");
            return (Criteria) this;
        }

        public Criteria andOrdersidNotBetween(String value1, String value2) {
            addCriterion("ORDERSID not between", value1, value2, "ordersid");
            return (Criteria) this;
        }

        public Criteria andItemsidIsNull() {
            addCriterion("ITEMSID is null");
            return (Criteria) this;
        }

        public Criteria andItemsidIsNotNull() {
            addCriterion("ITEMSID is not null");
            return (Criteria) this;
        }

        public Criteria andItemsidEqualTo(String value) {
            addCriterion("ITEMSID =", value, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidNotEqualTo(String value) {
            addCriterion("ITEMSID <>", value, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidGreaterThan(String value) {
            addCriterion("ITEMSID >", value, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMSID >=", value, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidLessThan(String value) {
            addCriterion("ITEMSID <", value, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidLessThanOrEqualTo(String value) {
            addCriterion("ITEMSID <=", value, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidLike(String value) {
            addCriterion("ITEMSID like", value, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidNotLike(String value) {
            addCriterion("ITEMSID not like", value, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidIn(List<String> values) {
            addCriterion("ITEMSID in", values, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidNotIn(List<String> values) {
            addCriterion("ITEMSID not in", values, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidBetween(String value1, String value2) {
            addCriterion("ITEMSID between", value1, value2, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsidNotBetween(String value1, String value2) {
            addCriterion("ITEMSID not between", value1, value2, "itemsid");
            return (Criteria) this;
        }

        public Criteria andItemsnumIsNull() {
            addCriterion("ITEMSNUM is null");
            return (Criteria) this;
        }

        public Criteria andItemsnumIsNotNull() {
            addCriterion("ITEMSNUM is not null");
            return (Criteria) this;
        }

        public Criteria andItemsnumEqualTo(String value) {
            addCriterion("ITEMSNUM =", value, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumNotEqualTo(String value) {
            addCriterion("ITEMSNUM <>", value, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumGreaterThan(String value) {
            addCriterion("ITEMSNUM >", value, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMSNUM >=", value, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumLessThan(String value) {
            addCriterion("ITEMSNUM <", value, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumLessThanOrEqualTo(String value) {
            addCriterion("ITEMSNUM <=", value, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumLike(String value) {
            addCriterion("ITEMSNUM like", value, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumNotLike(String value) {
            addCriterion("ITEMSNUM not like", value, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumIn(List<String> values) {
            addCriterion("ITEMSNUM in", values, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumNotIn(List<String> values) {
            addCriterion("ITEMSNUM not in", values, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumBetween(String value1, String value2) {
            addCriterion("ITEMSNUM between", value1, value2, "itemsnum");
            return (Criteria) this;
        }

        public Criteria andItemsnumNotBetween(String value1, String value2) {
            addCriterion("ITEMSNUM not between", value1, value2, "itemsnum");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}