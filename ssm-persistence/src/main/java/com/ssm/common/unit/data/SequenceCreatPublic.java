package com.ssm.common.unit.data;

import com.ssm.mapper.common.GetSequenceMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class SequenceCreatPublic {

    @Autowired
    private GetSequenceMapper getSequenceMapper;

    /**
     *
     * 返回Sequence值
     * @author yuze
     * @param sequencceName：sequence名称
     * @return String
     */
    public String getSeqValue(String sequencceName) throws Exception {
        return getSequenceMapper.getSequence(sequencceName);
    }
}
