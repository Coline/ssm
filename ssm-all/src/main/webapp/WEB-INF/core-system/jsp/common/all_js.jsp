<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%request.setAttribute("ctx",request.getContextPath());%>

<!-- Jquery引用 -->
<script src="${ctx}/core-system/depend/jquery/jquery.js" type="text/javascript"></script>
<script src="${ctx}/core-system/depend/jquery/jquery-1.9.1.js" type="text/javascript"></script>

<!-- layui引用 -->
<link rel="stylesheet" href="${ctx}/core-system/depend/layui/css/layui.css" media="all">
<script src="${ctx}/core-system/depend/layui/layui.js" type="text/javascript"></script>

<!-- 自定义插件 -->
<script src="${ctx}/core-system/js/layer/qui.layer.js" type="text/javascript"></script>