; qui = {
    // 打开弹出框
    openDialog : function(title,weight,height,url){//启动表单初始化
        layer.open({
            type: 2,
            title: title,// 标题
            shade: [0.8, '#393D49'],// 弹窗外区域遮盖层
            maxmin: true,// 最大最小化
            area: [weight, height],// 宽高
            offset: '15%',// 坐标
            shift: 2,
            content: [url, 'yes']//iframe的url，no代表不显示滚动条
        });
    }
};