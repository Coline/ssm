<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>标签维护</title>
    <script type="text/html" id="labelTableBar">
        <a class="layui-btn layui-btn-radius layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-radius layui-btn-normal layui-btn-xs" lay-event="adddPicture">添加图片</a>
        <a class="layui-btn layui-btn-radius layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
    <script>
        layui.use('table', function () {
            var table = layui.table;
            //第一个实例
            table.render({
                elem: '#labeltable'
                , height: 'full-200'
                , url: '${pageContext.request.contextPath }/cloth/queryLabel' //数据接口
                , page: true //开启分页
                , id: 'labeltableID'
                , cols: [[ //表头
                    {fixed: 'left', type: 'checkbox'}
                    , {field: 'serialNumber', title: '标签编号', width: "15%", align:'center', style:"text-align:center;"}
                    , {field: 'component', title: '成分', width: "15%", align:'center', style:"text-align:center;"}
                    , {field: 'warp', title: '经纱', width: "15%", align:'center', style:"text-align:center;"}
                    , {field: 'weft', title: '纬纱', width: "15%", align:'center', style:"text-align:center;"}
                    , {field: 'width', title: '门幅', width: "12%", align:'center', style:"text-align:center;"}
                    , {field: 'createDate', title: '创建时间', width: "10%", sort: true, align:'center', templet: '#titleTpl', style:"text-align:center;"}
                    , {fixed: 'right', title: '操作', width: "14%", align: 'center', toolbar: '#labelTableBar'}
                ]]
            });

            //监听工具条
            table.on('tool(labeltable)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
                var data = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                if (layEvent === 'del') { //删除
                    layer.confirm('真的删除行么', function (index) {
                        obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                        layer.close(index);
                        //向服务端发送删除指令
                        $.ajax({
                            type: "post"
                            , url: "${pageContext.request.contextPath }/cloth/labelDel"
                            , data: {ids: ","+data.id+","}
                            , success: function (resData) {
                                $("#labelMainSearchButton").click();
                                layer.msg(resData.message);
                            }
                        });
                    });
                } else if (layEvent === 'edit') {
                    qui.openDialog('修改标签信息', '45%', '78%', '${pageContext.request.contextPath}/cloth/labelUpdatePage/' + data.id);
                }else if (layEvent === 'adddPicture') {
                    qui.openDialog('添加图片', '50%', '65%', '${pageContext.request.contextPath}/cloth/labelAddPicture/' + data.id);
                }
            });

            var active = {
                // 查询方法
                search: function () {
                    // labeltableID是初始化table时的id，不是直接定义的
                    table.reload('labeltableID', {
                        where: { //设定异步数据接口的额外参数，任意设
                            name: $("input[name = 'serialNumber']").val()
                        }
                        , page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
                //批量删除方法
                , delChecked: function () {
                    var checkStatus = table.checkStatus('labeltableID');
                    var selectdataJson = checkStatus.data;// 选中数据
                    if(selectdataJson.length<=0){
                        layer.alert('请至少选择一个', {icon: 7});
                        return false;
                    }

                    layer.confirm('确定删除选中项？', function (index) {
                        var ids = ",";
                        $.each(selectdataJson, function (index, obj) {
                            ids += obj.id + ",";
                        });
                        //向服务端发送删除指令
                        $.ajax({
                            type: "post"
                            , url: "${pageContext.request.contextPath }/cloth/labelDel"
                            , data: {ids: ids}
                            , success: function (resData) {
                                $("#labelMainSearchButton").click();
                                layer.msg(resData.message);
                            }
                            ,complete: function () {
                                layer.close(index);
                            }
                        });
                    });
                }
            };

            // 按钮绑定方法
            $('.labelbut').on('click', function () {
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
            });
        });

    </script>
</head>
<body>
<div class="layui-row">
    <form class="layui-form" action="">
        <div class="layui-form-item">
            <label class="layui-form-label">标签编号</label>
            <div class="layui-input-inline">
                <input type="text" name="serialNumber" placeholder="请输入标签名称" autocomplete="off" class="layui-input">
            </div>
            <button class="layui-btn labelbut" type="button" data-type="search" id="labelMainSearchButton"><i
                    class="layui-icon">&#xe615;</i></button>
        </div>
    </form>
</div>
<div style="display: block;margin-bottom: 10px;padding: 5px;background-color: #f2f2f2;">
    <button class="layui-btn layui-btn-radius layui-btn-danger labelbut" data-type="delChecked">
        <i class="layui-icon labelbut">&#xe640;</i>
        批量删除
    </button>
    <button class="layui-btn layui-btn-radius"
            onclick="qui.openDialog('新增标签','45%','78%','${pageContext.request.contextPath }/cloth/labelAddPage');">
        <i class="layui-icon">&#xe608;</i>
        添加
    </button>
</div>
<table id="labeltable" lay-filter="labeltable"></table>
</body>
</html>