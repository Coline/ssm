<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
    .layui-upload-img {
        width: 92px;
        height: 92px;
        margin: 0 10px 10px 0;
    }
</style>
<head>
    <meta charset="utf-8">
    <title>标签添加</title>
    <script>
        // 提交按钮监听方法
        layui.use(['upload'], function () {
            var $ = layui.jquery, upload = layui.upload;
            //多图片上传
            upload.render({
                elem: '#fileArea'
                , url: '${pageContext.request.contextPath }/cloth/importLabelImportPicture'
                , multiple: true
                , acceptMime: 'image/*'
                , accept: 'images'
                , exts: 'jpg|png|gif|bmp|jpeg'
                , before: function (obj) {
                    this.data = {
                        id: $("input[name='id']").val()
                    };
                    obj.preview(function (index, file, result) {
                        $('#preview').append('<img src="' + result + '" alt="' + file.name + '" class="layui-upload-img">')
                    });
                }
                , done: function (res) {
                    layer.msg(res.message);
                }
            });
        });

        $(function () {
            $('#fileArea').css('display','');
        });
    </script>
</head>
<body>
    <div class="layui-row">
        <form class="layui-form layui-form-pane" id="labelAddFrom" action="">
            <div class="layui-form-item layui-form-text">
                <input type="text" name="id" value="${id}" class="layui-input layui-hide">
                <label class="layui-form-label" style="text-align: center">标签图片上传</label>
                <div class="layui-upload-drag" style="margin:0 auto;" id="fileArea">
                    <i class="layui-icon">&#xe67c;</i>
                    <p>点击上传，或将文件拖拽到此处</p>
                </div>
                <blockquote class="layui-elem-quote layui-quote-nm">
                    预览图：
                    <div class="layui-upload-list" id="preview"></div>
                </blockquote>
            </div>
        </form>
    </div>
</body>
</html>