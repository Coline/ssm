<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>标签添加</title>
    <script>
        // 提交按钮监听方法
        layui.use(['form', 'laydate'], function () {
            var form = layui.form;
            //监听提交
            form.on('submit(labelAddSubmitButton)', function (data) {
                addlabel_Submit();
                //阻止表单跳转。如果需要表单跳转，去掉这段即可
                return false;
            });
        });

        // 提交事件
        function addlabel_Submit() {
            $.ajax({
                type: "post"
                , url: "${pageContext.request.contextPath }/cloth/labelAdd"
                , data: $("#labelAddFrom").serializeArray()
                , success: function (resData) {
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    parent.layer.msg(resData.message);
                    // 调用父页面查询
                    window.parent.$("#labelMainSearchButton").click();
                }
                , error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
                , complete: function (XMLHttpRequest, textStatus) {
                }
            });
        };
    </script>
</head>
<body>
<div class="layui-row">
    <form class="layui-form layui-form-pane" id="labelAddFrom" action="">
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 130px;">标签编号</label>
            <div class="layui-input-inline">
                <input type="text" name="serialNumber" placeholder="请输入标签编号" autocomplete="off"
                       class="layui-input">
            </div>

            <label class="layui-form-label" style="width: 130px;">成分</label>
            <div class="layui-input-inline">
                <input type="text" name="component" placeholder="请输入成分" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 130px;">经纱</label>
            <div class="layui-input-inline">
                <input type="text" name="warp" placeholder="请输入经纱" autocomplete="off"
                       class="layui-input">
            </div>

            <label class="layui-form-label" style="width: 130px;">纬纱</label>
            <div class="layui-input-inline">
                <input type="text" name="weft" placeholder="请输入纬纱" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 130px;">经密</label>
            <div class="layui-input-inline">
                <input type="text" name="warpIntensity" placeholder="请输入经密" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label" style="width: 130px;">纬密</label>
            <div class="layui-input-inline">
                <input type="text" name="weftIntensity" placeholder="请输入纬密" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 130px;">门幅</label>
            <div class="layui-input-inline">
                <input type="text" name="width" placeholder="请输入门幅" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label" style="width: 130px;">克重</label>
            <div class="layui-input-inline">
                <input type="text" name="gramWeight" placeholder="请输入克重" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 130px;">供应商名称</label>
            <div class="layui-input-inline">
                <input type="text" name="supplierName" placeholder="请输入供应商名称" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label" style="width: 130px;">供应商报价</label>
            <div class="layui-input-inline">
                <input type="text" name="supplierQuotation" placeholder="请输入供应商面料编号" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 130px;">供应商面料编号</label>
            <div class="layui-input-inline">
                <input type="text" name="supplierFabricNumber" placeholder="请输入供应商面料编号" autocomplete="off"
                       class="layui-input">
            </div>
        </div>

        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">后整理</label>
            <div class="layui-input-block">
                <textarea name="postFinishing" placeholder="请输入后整理" class="layui-textarea"></textarea>
            </div>
        </div>

        <!-- 按钮区域 -->
        <div class="layui-form-item">
            <div class="layui-input-block" style="text-align:center">
                <button class="layui-btn" lay-submit lay-filter="labelAddSubmitButton">提交</button>
                <button class="layui-btn layui-btn-primary" type="reset">重置</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>