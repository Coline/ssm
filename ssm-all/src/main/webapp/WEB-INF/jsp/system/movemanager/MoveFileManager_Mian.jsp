<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>商品维护</title>
    <script>
        layui.use('table', function () {
            var table = layui.table;
            //第一个实例
            table.render({
                elem: '#movieFileTable'
                , height: 'full-200'
                , url: '${pageContext.request.contextPath }/moveFileManager/queryMoveFile' //数据接口
                , page: true //开启分页
                , id: 'movieFileTableID'
                , cols: [[ //表头
                    {field: 'fileName', title: '文件名称', width: "25%", align: 'center', style: "text-align:center;"}
                    , {field: 'filePath', title: '文件路径', width: "55%", align: 'center', style: "text-align:center;"}
                    , {
                        field: 'createDate',
                        title: '创建时间',
                        width: "20%",
                        sort: true,
                        align: 'center',
                        style: "text-align:center;"
                    }
                ]]
            });

            var active = {
                // 查询方法
                search: function () {
                    // movieFileTableID是初始化table时的id，不是直接定义的
                    table.reload('movieFileTableID', {
                        where: { //设定异步数据接口的额外参数，任意设
                            fileName: $("input[name = 'fileName']").val()
                            ,isRepeat: $("select[name = 'isRepeat']").val()
                        }
                        , page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }//批量删除方法
                , insertMovieFile: function () {
                    var filePath = $("input[name = 'insertFilePath']").val();
                    if (filePath != "" && filePath != "undefine") {
                        $.ajax({
                            type: "post"
                            , url: "${pageContext.request.contextPath }/moveFileManager/insertMoveFile"
                            , data: {filePath: filePath}
                            , success: function (resData) {
                                layer.msg(resData.message);
                            }
                        });
                    } else {
                        layer.alert('请填入参数', {icon: 7});
                    }

                }
                , dealRepeatMoveFile: function () {
                    $.ajax({
                        type: "get"
                        , url: "${pageContext.request.contextPath }/moveFileManager/dealRepeatMoveFile"
                        , data: {}
                        , success: function (resData) {
                            layer.msg(resData.message);
                        }
                    });
                }
            };

            // 按钮绑定方法
            $('.movieFileBut').on('click', function () {
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
            });
        });

    </script>
</head>
<body>
<div class="layui-row">
    <form class="layui-form" action="">
        <div class="layui-form-item">
            <label class="layui-form-label">商品名称</label>
            <div class="layui-input-inline">
                <input type="text" name="fileName" placeholder="请输入文件名称" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">是否重复</label>
                <div class="layui-input-inline">
                    <select name="isRepeat" lay-verify="required" lay-search="">
                        <option value=""></option>
                        <option value="1">是</option>
                        <option value="0">否</option>
                    </select>
                </div>
            </div>
            <button class="layui-btn movieFileBut" type="button" data-type="search" id="moveFileMainSearchButton"><i
                    class="layui-icon">&#xe615;</i></button>
        </div>
    </form>
</div>
<div style="display: block;margin-bottom: 10px;padding: 5px;background-color: #f2f2f2;">
    <button class="layui-btn layui-btn-radius layui-btn-danger movieFileBut" data-type="insertMovieFile">
        <i class="layui-icon itembut">&#xe6b1;</i>
        扫描文件
    </button>
    <div class="layui-input-inline">
        <input type="text" name="insertFilePath" placeholder="请输入文件路径" autocomplete="off" class="layui-input">
    </div>
    <button class="layui-btn layui-btn-radius layui-btn-danger movieFileBut" data-type="dealRepeatMoveFile">
        <i class="layui-icon itembut">&#xe630;</i>
        处理重复文件
    </button>
</div>
<table id="movieFileTable" lay-filter="movieFileTable"></table>
</body>
</html>