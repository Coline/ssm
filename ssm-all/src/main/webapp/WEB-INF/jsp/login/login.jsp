<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="../../core-system/jsp/common/all_js.jsp" %>
<html lang="zh-CN"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>赫拉克勒斯</title>

	<link rel="stylesheet" type="text/css" href="${ctx}/css/login/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/css/login/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/css/login/material-design-iconic-font.min.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/css/login/util.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/css/login/main.css">
</head>

<script src="${ctx}/core-system/depend/jquery/jquery-3.2.1.min.js"></script>
<script src="${ctx}/js/login/main.js"></script>

<body>
	<div class="limiter">
		<div class="container-login100" style="background-image: url(${ctx}/img/login/login-bg.jpg);">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<form class="layui-form layui-form-pane login100-form validate-form">
					<span class="login100-form-title p-b-49">登录</span>

					<div class="wrap-input100 validate-input m-b-23" data-validate="请输入用户名">
                        <i class="layui-icon" style="font-size: 20px;">&#xe66f;</i>
						<span class="label-input100">用户名</span>
						<input class="input100" type="text" name="username" placeholder="请输入用户名" autocomplete="off">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="请输入密码">
                        <i class="layui-icon" style="font-size: 20px;">&#xe673;</i>
                        <span class="label-input100">密码</span>
						<input class="input100" type="password" name="pass" placeholder="请输入密码">
						<span class="focus-input100"></span>
					</div>

					<div class="text-right p-t-8 p-b-31">
						<a href="javascript:">忘记密码？</a>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">登 录</button>
						</div>
					</div>

					<div class="txt1 text-center p-t-54 p-b-20">
						<span>第三方登录</span>
					</div>

					<div class="flex-c-m">
						<a href="" class="login100-social-item bg1">
                            <i class="layui-icon" style="font-size: 30px;">&#xe677;</i>
						</a>

						<a href="" class="login100-social-item bg2">
                            <i class="layui-icon" style="font-size: 30px;">&#xe676;</i>
						</a>

						<a href="" class="login100-social-item bg3">
                            <i class="layui-icon" style="font-size: 30px;">&#xe675;</i>
						</a>
					</div>

					<div class="flex-col-c p-t-25">
						<a href="javascript:" class="txt2">立即注册</a>
					</div>
				</form>
			</div>
		</div>
	</div>




</body></html>