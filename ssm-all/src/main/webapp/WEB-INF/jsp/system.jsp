<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>赫拉克勒斯</title>
</head>
<body>
<div class="layui-layout-admin">
    <!--头部-->
    <div class="layui-header">
        <div class="layui-logo">赫拉克勒斯</div>
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="javascript:void(0)">控制台</a></li>
            <li class="layui-nav-item"><a href="javascript:;">商品管理</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其他系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:;">邮件管理</a></dd>
                    <dd><a href="javascript:;">消息管理</a></dd>
                    <dd><a href="javascript:;">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="">YUZE</a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
        </ul>
    </div>

    <!--左侧-->
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <ul class="layui-nav layui-nav-tree" lay-filter="managerNavbar">
                <li class="layui-nav-item layui-nav-itemed">
                    <a href="javascript:;">商品后台维护</a>
                    <dl class="layui-nav-child">
                        <dd lay-id="商品维护">
                            <a href="javascript:;" data-options="{url:'${pageContext.request.contextPath}/items/itemsMain',title:'商品维护'}">商品维护</a>
                        </dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">流程管理</a>
                    <dl class="layui-nav-child">
                        <dd lay-id="流程模型管理">
                            <a href="javascript:;" data-options="{url:'${pageContext.request.contextPath}/modelManager/modelManagerView',title:'流程模型管理'}">流程模型管理</a>
                        </dd>
                        <dd lay-id="流程定义管理">
                            <a href="javascript:;" data-options="{url:'${pageContext.request.contextPath}/processManager/processManagerView',title:'流程定义管理'}">流程定义管理</a>
                        </dd>
                    </dl>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">电影管理</a>
                    <dl class="layui-nav-child">
                        <dd lay-id="电影文件管理">
                            <a href="javascript:;" data-options="{url:'${pageContext.request.contextPath}/moveFileManager/moveFileManagerView',title:'电影文件管理'}">电影文件管理</a>
                        </dd>
                    </dl>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">布料系统</a>
                    <dl class="layui-nav-child">
                        <dd lay-id="标签管理">
                            <a href="javascript:;" data-options="{url:'${pageContext.request.contextPath}/cloth/labelView',title:'标签管理'}">标签管理</a>
                        </dd>
                    </dl>
                    </dl>
                </li>
            </ul>
        </div>
    </div>

    <!--中间主体-->
    <div class="layui-body" id="container">
        <div class="layui-tab" lay-filter="tabs" lay-allowClose="true">
            <ul class="layui-tab-title">
                <li class="layui-this">首页</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item">首页内容</div>
            </div>
        </div>
    </div>

    <!--底部-->
    <div class="layui-footer">
        <center>于泽版权所有&copy;Tel:18258223748</center>
    </div>
</div>
<script>
    //JavaScript代码区域
    layui.use('element', function () {
        var element = layui.element;
        /*element.on('nav(managerNavbar)',function(elem){
            /!*使用DOM操作获取超链接的自定义data属性值*!/
           var options = eval('('+elem.context.children[0].dataset.options+')');
           var url = options.url;
           var title = options.title;
           element.tabAdd('tabs',{
               title : title,
               content : '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>',
               id : '111'
           });
       });*/
        /*使用下面的方式需要引用jquery*/
        $('.layui-nav-child a').click(function () {
            var options = eval('(' + $(this).data('options') + ')');
            var url = options.url;
            var title = options.title;

            // 用来判断选项卡是否已打开
            var flag = true;
            // 循环选项卡
            $("#container ul li").each(function () {
                // 若选项卡已打开则切换tab并设置flag为false
                if ($(this).attr("lay-id") == title) {
                    //切换到指定Tab项
                    element.tabChange('tabs', title); //切换
                    flag = false;
                    return false;
                }

            });
            if (flag) {
                element.tabAdd('tabs', {
                    title: title,
                    content: '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:90%;"></iframe>',
                    id: title
                });
                //切换到指定Tab项
                element.tabChange('tabs', title); //切换
            }
        });
    });
</script>
</body>
</html>
