<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>商品添加</title>
    <script>
        // 提交按钮监听方法
        layui.use(['form'], function () {
            var form = layui.form;
            //监听提交
            form.on('submit(itemUpdateSubmitButton)', function (data) {
                updateItems_Submit();
            });
        });

        // 提交事件
        function updateItems_Submit() {
            $.ajax({
                type: "post"
                , url: "${pageContext.request.contextPath }/items/itemsUpdate.action"
                , data: $("#itemsUpdateFrom").serializeArray()
                , success: function (resData) {
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    parent.layer.msg(resData.message);
                    // 调用父页面查询
                    window.parent.$("#itemsMainSearchButton").click();
                }
            });
        };
    </script>
</head>
<body>
<div class="layui-row">
    <form class="layui-form layui-form-pane" id="itemsUpdateFrom" action="">
        <c:if test="${fromData != null}">
            <div class="layui-form-item">
                <input type="text" name="id" value="${fromData.id}" class="layui-input layui-hide">
                <label class="layui-form-label">商品名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="name" lay-verify="required" placeholder="请输入商品名称" autocomplete="off"
                           value="${fromData.name}" class="layui-input">
                </div>

                <label class="layui-form-label">商品价格</label>
                <div class="layui-input-inline">
                    <input type="text" name="price" lay-verify="required" placeholder="请输入商品价格" autocomplete="off"
                           value="${fromData.price}" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-l-abel">商品描述</label>
                <div class="layui-input-block">
                    <textarea name="detail" placeholder="请输入商品描述" class="layui-textarea">${fromData.detail}</textarea>
                </div>
            </div>
        </c:if>
        <!-- 按钮区域 -->
        <div class="layui-form-item">
            <div class="layui-input-block" style="text-align:center">
                <button class="layui-btn" lay-submit lay-filter="itemUpdateSubmitButton">提交</button>
                <button class="layui-btn layui-btn-primary" type="reset">重置</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>