<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>商品添加</title>
    <script>
        // 提交按钮监听方法
        layui.use(['form','laydate'], function(){
            var form = layui.form;
            var laydate = layui.laydate;
            //监听提交
            form.on('submit(itemAddSubmitButton)', function(data){
                addItems_Submit();
                //阻止表单跳转。如果需要表单跳转，去掉这段即可
                return false;
            });

            //日期时间选择器
            laydate.render({
                elem: '#expirationdate'
                ,type: 'datetime'
                ,trigger: 'click'
            });
        });
        // 提交事件
        function  addItems_Submit() {
            $.ajax({
                type: "post"
                ,url: "${pageContext.request.contextPath }/items/itemsAdd"
                ,data: $("#itemsAddFrom").serializeArray()
                ,success: function (resData) {
                    console.log("success");
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    parent.layer.msg(resData.message);
                    // 调用父页面查询
                    window.parent.$("#itemsMainSearchButton").click();
                }
                ,error:function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.readyState);
                    console.log(textStatus);
                    console.log(errorThrown);
                    console.log("error");
                }
                ,complete:function (XMLHttpRequest, textStatus) {
                    console.log("complete");
                }
            });
        };
    </script>
</head>
<body>
<div class="layui-row">
    <form class="layui-form layui-form-pane" id="itemsAddFrom" action="">
        <div class="layui-form-item">
            <label class="layui-form-label">商品名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" lay-verify="required" placeholder="请输入商品名称" autocomplete="off" class="layui-input">
            </div>

            <label class="layui-form-label">商品价格</label>
            <div class="layui-input-inline">
                <input type="text" name="price" lay-verify="required" placeholder="请输入商品价格" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">是否生效</label>
            <div class="layui-input-block">
                <input type="radio" name="sfsx" value="1" title="是" checked="">
                <input type="radio" name="sfsx" value="0" title="否">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">有效期至</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" id="expirationdate" name = "expirationdate" placeholder="yyyy-MM-dd HH:mm:ss">
            </div>
        </div>

        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">商品描述</label>
            <div class="layui-input-block">
                <textarea name="detail" placeholder="请输入商品描述" class="layui-textarea"></textarea>
            </div>
        </div>

        <!-- 按钮区域 -->
        <div class="layui-form-item">
            <div class="layui-input-block" style="text-align:center">
                <button class="layui-btn" lay-submit lay-filter="itemAddSubmitButton">提交</button>
                <button class="layui-btn layui-btn-primary" type="reset">重置</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>