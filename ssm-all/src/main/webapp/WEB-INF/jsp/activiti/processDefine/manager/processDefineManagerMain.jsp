<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>商品维护</title>
    <script type="text/html" id="processDefinestableBar">
        <a class="layui-btn layui-btn-xs" lay-event="changeToModel">转化为模型</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
    <script>
        layui.use('table', function () {
            var table = layui.table;
            //第一个实例
            table.render({
                elem: '#processDefinestable'
                , height: 'full-200'
                , url: '${pageContext.request.contextPath }/processManager/queryProcessDefine' //数据接口
                , page: true //开启分页
                , id: 'processDefinestableID'
                , cols: [[ //表头
                    {fixed: 'left', type: 'checkbox'}
                    , {field: 'id', title: 'ID', width: "20%", align:'center', style:"text-align:center;"}
                    , {field: 'name', title: '名称', width: "15%", align:'center', style:"text-align:center;"}
                    , {field: 'key', title: 'KEY', width: "15%", align:'center', style:"text-align:center;"}
                    , {field: 'version', title: '版本', width: "5%", sort: true, align:'center', style:"text-align:center;"}
                    , {field: 'deploymentId', title: '部署ID', width: "10%", sort: true, align:'center', style:"text-align:center;"}
                    , {field: 'description', title: '描述', width: "20%", sort: true, align:'center', style:"text-align:center;"}
                    , {fixed: 'right', title: '操作', width: "10%", align: 'center', toolbar: '#processDefinestableBar'}
                ]]
            });

            //监听工具条
            table.on('tool(processDefinestable)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
                var data = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                if (layEvent === 'del') { //删除
                    layer.confirm('真的删除行么', function (index) {
                        obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                        layer.close(index);
                        //向服务端发送删除指令
                        $.ajax({
                            type: "delete"
                            , url: "${pageContext.request.contextPath }/processManager/deleteProcessDeployment/"+data.deploymentId
                            , success: function (resData) {
                                $("#processDefineMainSearchButton").click();
                                layer.msg(resData.message);
                            }
                        });
                    });
                } else if (layEvent == 'changeToModel') {

                    qui.openDialog('转化为可编辑Model','45%','70%','${pageContext.request.contextPath }/processManager/changeToModelView/'+data.deploymentId);

                } else if (layEvent == 'deployment') {
                    $.ajax({
                        type: "post"
                        , url: "${pageContext.request.contextPath }/processDefineManager/deployment/"+data.id
                        , data: {}
                        , success: function (resData) {
                            $("#processDefineMainSearchButton").click();
                            layer.msg(resData.message);
                        }
                    });
                }
            });

            var active = {
                // 查询方法
                search: function () {
                    // processDefinestableID是初始化table时的id，不是直接定义的
                    table.reload('processDefinestableID', {
                        where: { //设定异步数据接口的额外参数，任意设
                            name: $("input[name = 'name']").val()
                            ,key: $("input[name = 'key']").val()
                        }
                        , page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
                //批量删除方法
                , delChecked: function () {
                    var checkStatus = table.checkStatus('processDefinestableID');
                    var selectdataJson = checkStatus.data;// 选中数据
                    if(selectdataJson.length<=0){
                        layer.alert('请至少选择一个', {icon: 7});
                        return false;
                    }

                    layer.confirm('确定删除选中项？', function (index) {
                        var deploymentIds = "";
                        $.each(selectdataJson, function (index, obj) {
                            deploymentIds += obj.deploymentId + ",";
                        });
                        //向服务端发送删除指令
                        $.ajax({
                            type: "delete"
                            , url: "${pageContext.request.contextPath }/processManager/deleteProcessDeployment/"+deploymentIds
                            , success: function (resData) {
                                $("#processDefineMainSearchButton").click();
                                layer.msg(resData.message);
                            }
                            ,complete: function () {
                                layer.close(index);
                            }
                        });
                    });
                }
            };

            // 按钮绑定方法
            $('.itembut').on('click', function () {
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
            });
        });

    </script>
</head>
<body>
<div class="layui-row">
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">模型名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" placeholder="请输入模型名称" autocomplete="off" class="layui-input">
            </div>
            <label class="layui-form-label">模型Key</label>
            <div class="layui-input-inline">
                <input type="text" name="key" placeholder="请输入Key名称" autocomplete="off" class="layui-input">
            </div>
            <button class="layui-btn itembut" type="button" data-type="search" id="processDefineMainSearchButton"><i
                    class="layui-icon">&#xe615;</i></button>
        </div>
    </form>
</div>
<div style="display: block;margin-bottom: 10px;padding: 5px;background-color: #f2f2f2;">
    <button class="layui-btn layui-btn-radius layui-btn-danger itembut" data-type="delChecked">
        <i class="layui-icon itembut">&#xe640;</i>
        批量删除
    </button>
</div>
<table id="processDefinestable" lay-filter="processDefinestable"></table>
</body>
</html>