<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>Model添加</title>
    <script>
        // 提交按钮监听方法
        layui.use(['upload'], function () {
            var upload = layui.upload;
            //拖拽上传
            upload.render({
                elem: '#fileArea'
                , url: "${pageContext.request.contextPath }/modelManager/importModelByProcessResource"
                , method: 'post'
                , accept: 'file'
                , acceptMime: 'file/xml,file/bpmn'
                , exts: 'xml|bpmn'
                , auto: false
                , bindAction: '#modelAddSubmitButton'
                , before:function(obj){
                    layer.load(); //上传loading
                    this.data = {
                        name: $("input[name='name']").val()
                        ,key: $("input[name='key']").val()
                        ,description: $("textarea[name='description']").val()
                    };
                }
                , done: function (res) {
                    layer.closeAll('loading'); //关闭loading
                    window.open("${pageContext.request.contextPath }/modeler.html?modelId=" + res.Data);
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    parent.layer.msg(res.message);
                    // 调用父页面查询
                    window.parent.$("#modelMainSearchButton").click();
                }
            });
        });

    </script>
</head>
<body>
<div class="layui-row">
    <form class="layui-form layui-form-pane" id="modelImportForm">
        <div class="layui-form-item">
            <label class="layui-form-label">Model名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" lay-verify="required" placeholder="请输入Model名称" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label">Model Key</label>
            <div class="layui-input-inline">
                <input type="text" name="key" lay-verify="required" placeholder="请输入Model Key" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">Model描述</label>
            <div class="layui-input-block">
                <textarea name="description" placeholder="请输入Model描述" class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">Process定义xml文件</label>
            <div class="layui-upload-drag" id="fileArea">
                <i class="layui-icon">&#xe67c;</i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
        </div>

        <!-- 按钮区域 -->
        <div class="layui-form-item">
            <div class="layui-input-block" style="text-align:center">
                <button type="button" class="layui-btn" id="modelAddSubmitButton">提交</button>
                <button class="layui-btn layui-btn-primary" type="reset">重置</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>