<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>Model添加</title>
    <script>
        // 提交按钮监听方法
        layui.use(['form', 'laydate'], function () {
            var form = layui.form;
            var laydate = layui.laydate;
            //监听提交
            form.on('submit(modelAddSubmitButton)', function (data) {
                addItems_Submit();
                //阻止表单跳转。如果需要表单跳转，去掉这段即可
                return false;
            });
        });

        // 提交事件
        function addItems_Submit() {
            $.ajax({
                type: "post"
                , url: "${pageContext.request.contextPath }/modelManager/modelAdd"
                , data: $("#modelAddForm").serializeArray()
                , success: function (resData) {
                    window.open("${pageContext.request.contextPath }/modeler.html?modelId=" + resData.Data);
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    parent.layer.msg(resData.message);
                    // 调用父页面查询
                    window.parent.$("#modelMainSearchButton").click();
                }
                , error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.readyState);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
                , complete: function (XMLHttpRequest, textStatus) {
                }
            });
        };

    </script>
</head>
<body>
<div class="layui-row">
    <form class="layui-form layui-form-pane" id="modelAddForm">
        <div class="layui-form-item">
            <label class="layui-form-label">Model名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" lay-verify="required" placeholder="请输入Model名称" autocomplete="off"
                       class="layui-input">
            </div>
            <label class="layui-form-label">Model Key</label>
            <div class="layui-input-inline">
                <input type="text" name="key" lay-verify="required" placeholder="请输入Model Key" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">Model描述</label>
            <div class="layui-input-block">
                <textarea name="description" placeholder="请输入Model描述" class="layui-textarea"></textarea>
            </div>
        </div>

        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">Model初始化Json</label>
            <div class="layui-input-block">
                <textarea name="initBytesJson" placeholder="Model初始化json信息" class="layui-textarea"></textarea>
            </div>
        </div>

        <!-- 按钮区域 -->
        <div class="layui-form-item">
            <div class="layui-input-block" style="text-align:center">
                <button class="layui-btn" lay-submit lay-filter="modelAddSubmitButton">提交</button>
                <button class="layui-btn layui-btn-primary" type="reset">重置</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>