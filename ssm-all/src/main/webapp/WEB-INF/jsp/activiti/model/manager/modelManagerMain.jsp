<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ include file="../../../../core-system/jsp/common/all_js.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>商品维护</title>
    <script type="text/html" id="modelstableBar">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-xs" lay-event="deployment">部署</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
    <script>
        layui.use('table', function () {
            var table = layui.table;
            //第一个实例
            table.render({
                elem: '#modelstable'
                , height: 'full-200'
                , url: '${pageContext.request.contextPath }/modelManager/queryModels' //数据接口
                , page: true //开启分页
                , id: 'modelstableID'
                , cols: [[ //表头
                    {fixed: 'left', type: 'checkbox'}
                    , {field: 'id', title: 'ID', width: "10%", align:'center', style:"text-align:center;"}
                    , {field: 'name', title: '名称', width: "15%", align:'center', style:"text-align:center;"}
                    , {field: 'key', title: 'KEY', width: "15%", align:'center', style:"text-align:center;"}
                    , {field: 'createTime', title: '创建时间', width: "20%", sort: true, align:'center', style:"text-align:center;"}
                    , {field: 'lastUpdateTime', title: '更新时间', width: "20%", sort: true, align:'center', style:"text-align:center;"}
                    , {fixed: 'right', title: '操作', width: "10%", align: 'center', toolbar: '#modelstableBar'}
                ]]
            });

            //监听工具条
            table.on('tool(modelstable)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
                var data = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                if (layEvent === 'del') { //删除
                    layer.confirm('真的删除行么', function (index) {
                        obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                        layer.close(index);
                        //向服务端发送删除指令
                        $.ajax({
                            type: "post"
                            , url: "${pageContext.request.contextPath }/modelManager/deleteModels"
                            , data: {ids: data.id}
                            , success: function (resData) {
                                $("#modelMainSearchButton").click();
                                layer.msg(resData.message);
                            }
                        });
                    });
                } else if (layEvent == 'edit') {
                    window.open("${pageContext.request.contextPath }/modeler.html?modelId=" + data.id);
                } else if (layEvent == 'deployment') {
                    $.ajax({
                        type: "post"
                        , url: "${pageContext.request.contextPath }/modelManager/deployment/"+data.id
                        , data: {}
                        , success: function (resData) {
                            $("#modelMainSearchButton").click();
                            layer.msg(resData.message);
                        }
                    });
                }
            });

            var active = {
                // 查询方法
                search: function () {
                    // modelstableID是初始化table时的id，不是直接定义的
                    table.reload('modelstableID', {
                        where: { //设定异步数据接口的额外参数，任意设
                            name: $("input[name = 'name']").val()
                            ,key: $("input[name = 'key']").val()
                        }
                        , page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
                //批量删除方法
                , delChecked: function () {
                    var checkStatus = table.checkStatus('modelstableID');
                    var selectdataJson = checkStatus.data;// 选中数据
                    if(selectdataJson.length<=0){
                        layer.alert('请至少选择一个', {icon: 7});
                        return false;
                    }

                    layer.confirm('确定删除选中项？', function (index) {
                        var ids = "";
                        $.each(selectdataJson, function (index, obj) {
                            ids += obj.id + ",";
                        });
                        //向服务端发送删除指令
                        $.ajax({
                            type: "post"
                            , url: "${pageContext.request.contextPath }/modelManager/deleteModels"
                            , data: {ids: ids}
                            , success: function (resData) {
                                $("#modelMainSearchButton").click();
                                layer.msg(resData.message);
                            }
                            ,complete: function () {
                                layer.close(index);
                            }
                        });
                    });
                }
            };

            // 按钮绑定方法
            $('.itembut').on('click', function () {
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
            });
        });

    </script>
</head>
<body>
<div class="layui-row">
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">模型名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" placeholder="请输入模型名称" autocomplete="off" class="layui-input">
            </div>
            <label class="layui-form-label">模型Key</label>
            <div class="layui-input-inline">
                <input type="text" name="key" placeholder="请输入Key名称" autocomplete="off" class="layui-input">
            </div>
            <button class="layui-btn itembut" type="button" data-type="search" id="modelMainSearchButton"><i
                    class="layui-icon">&#xe615;</i></button>
        </div>
    </form>
</div>
<div style="display: block;margin-bottom: 10px;padding: 5px;background-color: #f2f2f2;">
    <button class="layui-btn layui-btn-radius layui-btn-danger itembut" data-type="delChecked">
        <i class="layui-icon itembut">&#xe640;</i>
        批量删除
    </button>
    <button class="layui-btn layui-btn-radius"
            onclick="qui.openDialog('新增Model','45%','70%','${pageContext.request.contextPath }/modelManager/modelAddPage');">
        <i class="layui-icon">&#xe608;</i>
        添加
    </button>
    <button class="layui-btn layui-btn-radius"
            onclick="qui.openDialog('使用Process定义文件导入Model','45%','70%','${pageContext.request.contextPath }/modelManager/importModelByProcessResourceView');">
        <i class="layui-icon">&#xe663;</i>
        导入
    </button>
</div>
<table id="modelstable" lay-filter="modelstable"></table>
</body>
</html>