package com.ssm.test.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class Book {

    @Before(value = "execution(* com.ssm.test.aop.Shop.have(..))")
    public void before() {
        System.out.println("+++++++++++++++++++++++++before+++++++++++++++++++++++++++");
    }
    public void after(){
        System.out.println("+++++++++++++++++++++++++after+++++++++++++++++++++++++++");
    }

    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("+++++++++++++++++++++++++方法之前+++++++++++++++++++++++++++");
        proceedingJoinPoint.proceed();
        System.out.println("+++++++++++++++++++++++++方法之后+++++++++++++++++++++++++++");
    }
}
