package com.ssm.test.aop;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestAop {

    @Test
    public void testaop(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("conf/spring/applicationContext-test.xml");
        Shop shop = (Shop) context.getBean("shop");
        shop.have();
    }

}
