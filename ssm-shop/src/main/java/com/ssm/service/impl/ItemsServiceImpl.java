package com.ssm.service.impl;

import com.ssm.common.unit.data.SequenceCreatPublic;
import com.ssm.mapper.shop.ItemsMapperCustom;
import com.ssm.po.shop.Items;
import com.ssm.service.ItemsService;
import com.ssm.vo.shop.ItemsCustom;
import com.ssm.vo.shop.ItemsQueryVo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 
 * <p>Title: ItemsServiceImpl</p>
 * <p>Description: 商品管理</p>
 * @author yuze
 */
public class ItemsServiceImpl implements ItemsService {
	
	@Autowired
	private ItemsMapperCustom itemsMapperCustom;
	@Autowired
	private SequenceCreatPublic sequenceCreatPublic;

	/**
	 *
	 * 查询商品
	 * @param itemsQueryVo：查询条件
	 * @return List<ItemsCustom>
	 * @throws Exception：Exception
	 */
	@Override
	public List<ItemsCustom> findItemsList(ItemsQueryVo itemsQueryVo) throws Exception {
		Integer page = itemsQueryVo.getPage();
		Integer limit = itemsQueryVo.getLimit();
		itemsQueryVo.setBegincount((page-1)*limit+1);
		itemsQueryVo.setEndcount(page*limit);
		//通过ItemsMapperCustom查询数据库
		return itemsMapperCustom.findItemsList(itemsQueryVo);
	}

	/**
	 *
	 * 查询商品总数
	 * @param itemsQueryVo ：查询条件
	 * @return Integer
	 * @throws Exception：Exception
	 */
	@Override
	public Integer findItemsCount(ItemsQueryVo itemsQueryVo) throws Exception {
		//通过ItemsMapperCustom查询数据库
		return itemsMapperCustom.findItemsCount(itemsQueryVo);
	}

	/**
	 * 插入商品信息
	 * @param items:商品PO
	 * @throws Exception：Exception异常
	 */
	@Override
	public void insertItems(Items items) throws Exception{
		items.setId(sequenceCreatPublic.getSeqValue("item_seq"));
		itemsMapperCustom.insertItems(items);
	}

	/**
	 * 删除商品信息
	 * @param id:id
	 * @throws Exception：Exception异常
	 */
	@Override
	public void deleteItems(String id) throws Exception{
		itemsMapperCustom.deleteItems(id);
	}
	/**
	 * 批量删除商品
	 * @param ids:商品id逗号拼接字符串
	 * @throws Exception：Exception异常
	 */
	@Override
	public void deleteCheckedItems(String ids)throws Exception{
		itemsMapperCustom.deleteCheckedItems(ids);
	}
	/**
	 * 获取商品信息
	 * @param id:商品id
	 * @throws Exception：Exception异常
	 */
	@Override
	public Items getItems(String id)throws Exception{
		return itemsMapperCustom.getItems(id);
	}
	/**
	 * 修改商品信息
	 * @param items:商品PO
	 * @throws Exception：Exception异常
	 */
	@Override
	public void updateItems(Items items)throws Exception{
		itemsMapperCustom.updateItems(items);
	}
}
