package com.ssm.controller;


import com.ssm.common.util.layui.LayuiUnit;
import com.ssm.common.util.system.ISsmResponse;
import com.ssm.po.shop.Items;
import com.ssm.service.ItemsService;
import com.ssm.vo.shop.ItemsCustom;
import com.ssm.vo.shop.ItemsQueryVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>Title: ItemsController</p>
 * <p>Description:商品的controller</p>
 *
 * @author yuze
 */
@Controller
@RequestMapping("/items")
public class ItemsController {
    private static Logger logger = LoggerFactory.getLogger(ItemsController.class);
    @Resource
    private ItemsService itemsService;
    @Resource
    private RedisTemplate<String, String> template;

    /**
     * 商品维护主页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/itemsMain")
    public ModelAndView itemsMain() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("items/itemsList");
        return modelAndView;
    }

    /**
     * 商品添加页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/itemsAddPage")
    public ModelAndView itemsAddPage() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("items/itemsList_add");
        return modelAndView;
    }

    /**
     * 商品添加
     *
     * @return String
     * @throws Exception:Exception异常
     */
    @RequestMapping(value = {"/itemsAdd"}, method = {RequestMethod.POST})
    public @ResponseBody
    ISsmResponse itemsAdd(Items items) throws Exception {
        ISsmResponse res = new ISsmResponse();
        // 添加商品
        itemsService.insertItems(items);

        template.opsForValue().set("itemnage", items.getName());
        logger.info("val {}", template.opsForValue().get("itemnage"));

        res.setMessage(res, "添加成功");
        return res;
    }

    /**
     * 商品删除
     *
     * @return String
     * @throws Exception:Exception异常
     */
    @RequestMapping("/itemsDel")
    public @ResponseBody
    ISsmResponse itemsDel(String id) throws Exception {
        ISsmResponse res = new ISsmResponse();
        //删除商品
        itemsService.deleteItems(id);
        res.setMessage(res, "删除成功");
        return res;
    }

    /**
     * 批量删除商品
     *
     * @param ids:商品id逗号拼接字符串
     * @throws Exception：Exception异常
     */
    @RequestMapping("/deleteCheckedItems")
    public @ResponseBody
    ISsmResponse deleteCheckedItems(String ids) throws Exception {
        ISsmResponse res = new ISsmResponse();
        itemsService.deleteCheckedItems(ids);
        res.setMessage(res, "批量删除成功");
        return res;
    }

    /**
     * 商品修改页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/itemsUpdatePage")
    public ModelAndView itemsUpdatePage(String id) throws Exception {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 获取商品信息
        modelAndView.addObject("fromData", itemsService.getItems(id));
        // 指定视图
        modelAndView.setViewName("items/itemsList_update");
        return modelAndView;
    }

    /**
     * 商品修改
     *
     * @return ModelAndView
     */
    @RequestMapping("/itemsUpdate")
    public @ResponseBody
    ISsmResponse itemsUpdate(Items items) throws Exception {
        ISsmResponse res = new ISsmResponse();
        itemsService.updateItems(items);
        res.setMessage(res, "修改成功");
        return res;
    }

    /**
     * 商品查询
     *
     * @return LayuiUnit
     * @throws Exception:Exception异常
     */
    @RequestMapping("/queryItems")
    public @ResponseBody
    LayuiUnit queryItems(ItemsQueryVo itemsQueryVo) throws Exception {
        String name = itemsQueryVo.getName();
        if (name != "" && name != null) {
            itemsQueryVo.setName("%" + name + "%");
        }
        // 调用service查找 数据库，查询商品列表
        List<ItemsCustom> itemsList = itemsService.findItemsList(itemsQueryVo);

        return LayuiUnit.data(itemsService.findItemsCount(itemsQueryVo), itemsList);
    }

}
