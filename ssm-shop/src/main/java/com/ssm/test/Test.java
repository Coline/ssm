package com.ssm.test;

import java.util.*;

class Test {
    public static void main(String[] args){
        HashMap<Integer,User> users = new HashMap<>(3);
        users.put(1,new User("张三",11));
        users.put(2,new User("李四",13));
        users.put(3,new User("王五",12));
        System.out.print(users);

        HashMap<Integer,User> sortHashmap = sortHashMap(users);
        System.out.print(sortHashmap);
    }

    public static HashMap<Integer,User> sortHashMap(HashMap<Integer,User> users){
        Set<Map.Entry<Integer, User>> entrySet = users.entrySet();

        List<Map.Entry<Integer, User>> list = new ArrayList<>(entrySet);

        Collections.sort(list, new Comparator<Map.Entry<Integer, User>>() {
            @Override
            public int compare(Map.Entry<Integer, User> o1, Map.Entry<Integer, User> o2) {
                return o2.getValue().getAge()-o1.getValue().getAge();
            }
        });

        LinkedHashMap<Integer,User> linkedHashMap = new LinkedHashMap<>();

        for(Map.Entry<Integer, User> entry:list){
            linkedHashMap.put(entry.getKey(),entry.getValue());
        }

        return linkedHashMap;
    }

}
