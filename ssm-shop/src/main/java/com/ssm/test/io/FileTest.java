package com.ssm.test.io;

import java.io.*;

public class FileTest {

    public static void main(String[] args0){
        File file = new File("D:\\D盘文件夹说明.txt");
        BufferedReader bufferedReader = null;
        try {
            Reader reader =  new FileReader(file);
            bufferedReader = new BufferedReader(reader);

            String data = null;
            while ((data = bufferedReader.readLine()) != null){
                System.out.println(data);
            }
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
