package com.ssm.test.thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestRunable implements Runnable {
    private String name;
    private Lock lock = new ReentrantLock();

    public TestRunable(String name) {
        this.name=name;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "运行  :  " + name);
        try {
            Thread.sleep((int) Math.random() * 10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        for (int i = 0; i < 5; i++) {
//            System.out.println(name + "运行  :  " + i);
//            try {
//                Thread.sleep((int) Math.random() * 10);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
    }


}
