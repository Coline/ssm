package com.ssm.external.wechat.cloth.label.api;

import com.ssm.common.model.external.wechart.cloth.QueryLabelModel;
import com.ssm.common.model.external.wechart.cloth.QueryLabelsModel;
import com.ssm.external.wechat.cloth.label.service.LabelMianService;
import com.ssm.vo.cloth.label.ClothLabelQueryVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/external/wechat/cloth")
public class LabelMain {
    /**
     * 标签查询
     */
    @RequestMapping("/queryLabel")
    public QueryLabelsModel queryLabel(ClothLabelQueryVo clothLabelQueryVo) {
        String serialNum = clothLabelQueryVo.getSerialNumber();
        if (StringUtils.isNotEmpty(serialNum)) {
            clothLabelQueryVo.setSerialNumber("%" + serialNum + "%");
        }
        QueryLabelsModel queryLabelModel = new QueryLabelsModel();
        // 调用service查找 数据库，查询标签列表
        queryLabelModel.setCount(labelMianService.findLabelCount(clothLabelQueryVo));
        queryLabelModel.setData(labelMianService.findLabelList(clothLabelQueryVo));
        return queryLabelModel;
    }

    /**
     * 根据主键查询
     */
    @RequestMapping("/queryLabelByPk")
    public QueryLabelModel queryLabelByPk(String id) {
        QueryLabelModel queryLabelModel = new QueryLabelModel();
        queryLabelModel.setClothLabelPo(labelMianService.getLabelByParamKey(id));
        queryLabelModel.setLabelPicturePos(labelMianService.getLabelPicturesByLabelId(id));
        return queryLabelModel;
    }

    @Resource
    private LabelMianService labelMianService;

}
