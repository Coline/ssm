package com.ssm.external.wechat.cloth.label.service;

import com.ssm.mapper.cloth.label.ClothLabelMapper;
import com.ssm.mapper.cloth.label.LabelPictureMapper;
import com.ssm.po.cloth.label.ClothLabelPo;
import com.ssm.po.cloth.label.LabelPicturePo;
import com.ssm.vo.cloth.label.ClothLabelQueryVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LabelMianService {
    /**
     * 查询标签
     */
    public List<ClothLabelPo> findLabelList(ClothLabelQueryVo clothLabelQueryVo) {
        Integer page = clothLabelQueryVo.getPage();
        Integer limit = clothLabelQueryVo.getLimit();
        clothLabelQueryVo.setBegincount((page-1)*limit+1);
        clothLabelQueryVo.setEndcount(page*limit);
        //通过ItemsMapperCustom查询数据库
        return clothLabelMapper.queryClothLabel(clothLabelQueryVo);
    }

    /**
     * 查询总数
     */
    public Integer findLabelCount(ClothLabelQueryVo clothLabelQueryVo) {
        return clothLabelMapper.getClothLabelCount(clothLabelQueryVo);
    }

    /**
     * 根据主键获取标签信息
     */
    public ClothLabelPo getLabelByParamKey(String id){
        // 根据主键获取标签信息
        return clothLabelMapper.getClothLabelByParamKey(id);
    }

    /**
     * 根据主键获取标签信息
     */
    public List<LabelPicturePo> getLabelPicturesByLabelId(String labelId){
        // 根据label主键获取图片信息
        return labelPictureMapper.getLabelPicturePictureByLabelId(labelId);
    }

    @Resource
    private ClothLabelMapper clothLabelMapper;
    @Resource
    private LabelPictureMapper labelPictureMapper;

}
