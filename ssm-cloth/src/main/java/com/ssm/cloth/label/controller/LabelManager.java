package com.ssm.cloth.label.controller;

import com.ssm.cloth.label.service.LabelManagerService;
import com.ssm.common.util.layui.LayuiUnit;
import com.ssm.common.util.system.ISsmResponse;
import com.ssm.po.cloth.label.ClothLabelPo;
import com.ssm.vo.cloth.label.ClothLabelQueryVo;
import com.ssm.vo.cloth.label.LabelPicturelAddVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * 标签管理
 *
 * @author yuze
 */
@RestController
@RequestMapping("/cloth")
public class LabelManager {
    /**
     * 标签管理主页面
     *
     * @return loginView
     */
    @RequestMapping("/labelView")
    public ModelAndView labelView() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("/cloth/label/label_main");
        return modelAndView;
    }

    /**
     * 标签添加页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/labelAddPage")
    public ModelAndView labelAddPage() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("/cloth/label/label_add");
        return modelAndView;
    }

    /**
     * 标签添加
     */
    @RequestMapping(value = {"/labelAdd"}, method = {RequestMethod.POST})
    public ISsmResponse labelAdd(ClothLabelPo clothLabelPo) {
        ISsmResponse res = new ISsmResponse();
        // 添加标签
        labelManagerService.labelAdd(clothLabelPo);

        res.setMessage(res, "添加成功");
        return res;
    }

    /**
     * 修改页面
     */
    @RequestMapping("/labelUpdatePage/{id}")
    public ModelAndView labelUpdatePage(@PathVariable("id") String id) {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 获取标签信息
        modelAndView.addObject("fromData", labelManagerService.getLabelByParamKey(id));
        // 指定视图
        modelAndView.setViewName("/cloth/label/label_update");
        return modelAndView;
    }

    /**
     * 标签修改
     *
     * @return ModelAndView
     */
    @RequestMapping(value = {"/labelUpdate"}, method = {RequestMethod.POST})
    public @ResponseBody
    ISsmResponse labelUpdate(ClothLabelPo clothLabelPo) {
        ISsmResponse res = new ISsmResponse();
        labelManagerService.updateLabel(clothLabelPo);
        res.setMessage(res, "修改成功");
        return res;
    }

    /**
     * 标签删除
     *
     * @return String
     * @throws Exception:Exception异常
     */
    @RequestMapping(value = {"/labelDel"}, method = {RequestMethod.POST})
    public ISsmResponse labelDel(String ids) {
        ISsmResponse res = new ISsmResponse();
        //删除标签
        labelManagerService.deleteLabel(ids);
        res.setMessage(res, "删除成功");
        return res;
    }

    /**
     * 标签查询
     */
    @RequestMapping("/queryLabel")
    public LayuiUnit queryLabel(ClothLabelQueryVo clothLabelQueryVo) {
        String serialNum = clothLabelQueryVo.getSerialNumber();
        if (StringUtils.isNotEmpty(serialNum)) {
            clothLabelQueryVo.setSerialNumber("%" + serialNum + "%");
        }
        // 调用service查找 数据库，查询标签列表
        List<ClothLabelPo> labelList = labelManagerService.findLabelList(clothLabelQueryVo);

        return LayuiUnit.data(labelManagerService.findLabelCount(clothLabelQueryVo), labelList);
    }

    /**
     * 标签添加页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/labelAddPicture/{id}")
    public ModelAndView labelAddPicture(@PathVariable("id") String id) {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 获取标签信息
        modelAndView.addObject("id", id);
        // 指定视图
        modelAndView.setViewName("/cloth/label/label_add_picture");
        return modelAndView;
    }

    /**
     * Model导入
     * @param file
     * @param actReModelAddVo
     * @return
     * @throws IOException
     */
    @RequestMapping("/importLabelImportPicture")
    public @ResponseBody ISsmResponse importLabelImportPicture(MultipartFile file, LabelPicturelAddVo actReModelAddVo) throws IOException {
        ISsmResponse res = new ISsmResponse();
        labelManagerService.importModelByProcessResource(file, actReModelAddVo.getId());
        res.setMessage(res, "导入成功");
        return res;
    }

    @Resource
    private LabelManagerService labelManagerService;

}
