package com.ssm.cloth.label.service;

import com.ssm.common.util.PropertyManager;
import com.ssm.common.util.file.DeleteFileUtil;
import com.ssm.common.util.file.UploadFile;
import com.ssm.mapper.cloth.label.ClothLabelMapper;
import com.ssm.mapper.cloth.label.LabelPictureMapper;
import com.ssm.po.cloth.label.ClothLabelPo;
import com.ssm.po.cloth.label.LabelPicturePo;
import com.ssm.vo.cloth.label.ClothLabelQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * 标签管理Service层
 */
@Service
public class LabelManagerService {

    /**
     * 标签添加
     */
    public void labelAdd(ClothLabelPo clothLabelPo){
        String viewTotal = "【编号】："+clothLabelPo.getSerialNumber();
        viewTotal += "\n【成分】："+clothLabelPo.getComponent();
        viewTotal += "\n【经纱】："+clothLabelPo.getWarp();
        viewTotal += "\n【纬纱】："+clothLabelPo.getWeft();
        viewTotal += "\n【经密】："+clothLabelPo.getWarpIntensity();
        viewTotal += "\n【纬密】："+clothLabelPo.getWeftIntensity();
        viewTotal += "\n【门幅】："+clothLabelPo.getWidth();
        viewTotal += "\n【克重】："+clothLabelPo.getGramWeight();
        viewTotal += "\n【后整理】："+clothLabelPo.getPostFinishing();
        clothLabelPo.setViewTotal(viewTotal);
        // 添加商品
        clothLabelMapper.insert(clothLabelPo);

    }

    /**
     * 查询标签
     */
    public List<ClothLabelPo> findLabelList(ClothLabelQueryVo clothLabelQueryVo) {
        Integer page = clothLabelQueryVo.getPage();
        Integer limit = clothLabelQueryVo.getLimit();
        clothLabelQueryVo.setBegincount((page-1)*limit+1);
        clothLabelQueryVo.setEndcount(page*limit);
        //通过ItemsMapperCustom查询数据库
        return clothLabelMapper.queryClothLabel(clothLabelQueryVo);
    }

    /**
     * 查询总数
     */
    public Integer findLabelCount(ClothLabelQueryVo clothLabelQueryVo) {
        return clothLabelMapper.getClothLabelCount(clothLabelQueryVo);
    }

    /**
     * 根据主键获取标签信息
     */
    public ClothLabelPo getLabelByParamKey(String id){
        // 根据主键获取标签信息
        return clothLabelMapper.getClothLabelByParamKey(id);
    }

    /**
     * 根据主键获取标签信息
     */
    public void updateLabel(ClothLabelPo clothLabelPo){
        String viewTotal = "【编号】："+clothLabelPo.getSerialNumber();
        viewTotal += "\n【成分】："+clothLabelPo.getComponent();
        viewTotal += "\n【经纱】："+clothLabelPo.getWarp();
        viewTotal += "\n【纬纱】："+clothLabelPo.getWeft();
        viewTotal += "\n【经密】："+clothLabelPo.getWarpIntensity();
        viewTotal += "\n【纬密】："+clothLabelPo.getWeftIntensity();
        viewTotal += "\n【门幅】："+clothLabelPo.getWidth();
        viewTotal += "\n【克重】："+clothLabelPo.getGramWeight();
        viewTotal += "\n【后整理】："+clothLabelPo.getPostFinishing();
        clothLabelPo.setViewTotal(viewTotal);
        // 根据主键获取标签信息
        clothLabelMapper.updateLabel(clothLabelPo);
    }

    /**
     * 删除标签
     */
    public void deleteLabel(String ids){
        List<LabelPicturePo> labelPictures = labelPictureMapper.getLabelPicturePictureByLabelIds(ids);
        String basePath = PropertyManager.getProperty("label_picture_base_path", null);
        labelPictures.forEach(labelPicturePo ->{
            String fileSrc = labelPicturePo.getPictureSrc();
            DeleteFileUtil.deleteFile(basePath+fileSrc);
        });
        labelPictureMapper.deletePictureByLabelIds(ids);
        clothLabelMapper.deleteLabel(ids);
    }

    /**
     * 导入图片
     *
     * @param file
     * @return
     */
    public void importModelByProcessResource(MultipartFile file, String labelId) {
        String pictureSrc = UploadFile.saveFileAndChangeName(file);
        LabelPicturePo labelPicturePo = new LabelPicturePo();
        labelPicturePo.setLabelId(labelId);
        labelPicturePo.setPictureName(file.getOriginalFilename());
        labelPicturePo.setPictureSrc(pictureSrc);
        labelPictureMapper.insert(labelPicturePo);
    }


    @Resource
    private ClothLabelMapper clothLabelMapper;
    @Resource
    private LabelPictureMapper labelPictureMapper;
}
