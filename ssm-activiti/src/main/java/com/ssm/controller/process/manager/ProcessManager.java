package com.ssm.controller.process.manager;

import com.ssm.common.util.layui.LayuiUnit;
import com.ssm.common.util.system.ISsmResponse;
import com.ssm.service.manager.ProcessManagerService;
import com.ssm.vo.activiti.manager.BaseManagerQueryVo;
import com.ssm.vo.activiti.manager.ProcessDefineModel;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * 流程定义管理
 *
 * @author yuze
 */
@RestController
@RequestMapping("/processManager")
public class ProcessManager {
    @Resource
    ProcessManagerService processManagerService;

    /**
     * 流程定义管理主页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/processManagerView")
    public ModelAndView processManagerView() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("/activiti/processDefine/manager/processDefineManagerMain");
        return modelAndView;
    }

    /**
     * Activiti Process Define 查询
     *
     * @return LayuiUnit
     */
    @RequestMapping("/queryProcessDefine")
    public @ResponseBody LayuiUnit queryModels(BaseManagerQueryVo baseManagerQueryVo) {
        return processManagerService.queryActivitiProcessDefine(baseManagerQueryVo);
    }

    /**
     * process 转化为可编辑Model页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/changeToModelView/{deploymentId}")
    public ModelAndView changeToModelView(@PathVariable String deploymentId) {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 获取商品信息
        modelAndView.addObject("obj", deploymentId);
        // 指定视图
        modelAndView.setViewName("/activiti/processDefine/manager/processDefineManager_changeToModel");
        return modelAndView;
    }

    /**
     * process 转化为可编辑Model
     *
     * @return String
     */
    @RequestMapping(value = {"/changeToModel"}, method = {RequestMethod.POST})
    public @ResponseBody ISsmResponse changeToModel(ProcessDefineModel actReModelAddVo) {
        ISsmResponse res = new ISsmResponse();
        res.setDate(res, processManagerService.changeProcessToModel(actReModelAddVo));
        res.setMessage(res, "添加成功");
        return res;
    }
    /**
     * process 删除部署流程
     *
     * @return String
     */
    @RequestMapping(value = {"/deleteProcessDeployment/{deploymentIds}"}, method = {RequestMethod.DELETE})
    public @ResponseBody ISsmResponse deleteProcessDeployment(@PathVariable String deploymentIds) {
        ISsmResponse res = new ISsmResponse();
        processManagerService.deleteProcessDeployments(deploymentIds,true);
        res.setMessage(res, "删除成功");
        return res;
    }

}
