package com.ssm.controller.model.manager;

import com.ssm.common.util.layui.LayuiUnit;
import com.ssm.common.util.system.ISsmResponse;
import com.ssm.service.manager.ModelManagerService;
import com.ssm.vo.activiti.manager.BaseManagerQueryVo;
import com.ssm.vo.activiti.manager.BaseModelAddVo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 流程设计管理
 *
 * @author yuze
 */
@RestController
@RequestMapping("/modelManager")
public class ModelManage {
    @Resource
    private ModelManagerService modelManagerService;


    /**
     * 流程设计管理主页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/modelManagerView")
    public ModelAndView modelManagerView() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("/activiti/model/manager/modelManagerMain");
        return modelAndView;
    }

    /**
     * Activiti Moodel查询
     *
     * @return LayuiUnit
     * @throws Exception:Exception异常
     */
    @RequestMapping("/queryModels")
    public @ResponseBody
    LayuiUnit queryModels(BaseManagerQueryVo modelManagerQueryVo) {
        return modelManagerService.queryActivitiModel(modelManagerQueryVo);
    }

    /**
     * Model添加页面
     *
     * @return ModelAndView
     */
    @RequestMapping("/modelAddPage")
    public ModelAndView modelAddPage() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("activiti/model/manager/modelManager_add");
        return modelAndView;
    }

    /**
     * Model添加
     *
     * @return String
     * @throws Exception:Exception异常
     */
    @RequestMapping(value = {"/modelAdd"}, method = {RequestMethod.POST})
    public @ResponseBody
    ISsmResponse modelAdd(BaseModelAddVo actReModelAddVo) throws Exception {
        ISsmResponse res = new ISsmResponse();
        res.setDate(res, modelManagerService.saveModel(actReModelAddVo));
        res.setMessage(res, "添加成功");
        return res;
    }

    /**
     * Model部署
     *
     * @return String
     * @throws Exception:Exception异常
     */
    @RequestMapping(value = {"/deployment/{modelId}"}, method = {RequestMethod.POST})
    public @ResponseBody ISsmResponse deployment(@PathVariable String modelId) throws Exception {
        ISsmResponse res = new ISsmResponse();
        modelManagerService.deployment(modelId);
        res.setMessage(res, "部署成功");
        return res;
    }

    /**
     * 批量删除商品
     *
     * @param ids:商品id逗号拼接字符串
     * @throws Exception：Exception异常
     */
    @RequestMapping("/deleteModels")
    public @ResponseBody
    ISsmResponse deleteModels(String ids) {
        ISsmResponse res = new ISsmResponse();
        modelManagerService.deleteModels(ids);
        res.setMessage(res, "删除成功");
        return res;
    }

    /**
     * Model导入页面
     * @return
     */
    @RequestMapping("/importModelByProcessResourceView")
    public ModelAndView importModelByProcessResourceView() {
        // 返回ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // 指定视图
        modelAndView.setViewName("/activiti/model/manager/modelManager_importModelByProcessSource");
        return modelAndView;
    }

    /**
     * Model导入
     * @param file
     * @param actReModelAddVo
     * @return
     * @throws IOException
     */
    @RequestMapping("/importModelByProcessResource")
    public @ResponseBody ISsmResponse importModelByProcessResource(MultipartFile file,BaseModelAddVo actReModelAddVo) throws IOException {
        ISsmResponse res = new ISsmResponse();
        res.setDate(res, modelManagerService.importModelByProcessResource(file, actReModelAddVo));
        res.setMessage(res, "导入成功");
        return res;
    }

}
