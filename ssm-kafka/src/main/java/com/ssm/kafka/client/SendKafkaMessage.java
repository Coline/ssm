package com.ssm.kafka.client;

import com.ssm.kafka.listern.producer.KafkaProducerServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

//@Controller
//@RequestMapping("/kafka")
public class SendKafkaMessage {
    @Resource
    private KafkaProducerServer kafkaProducerServer;
    /**
     * 商品查询
     * @return LayuiUnit
     * @throws Exception:Exception异常
     */
    @RequestMapping(value = "/sendMess", method = RequestMethod.GET)
    public @ResponseBody String sendMess() {

        String topic = "ssm_java_test1";
        String value = "我就是要看到val";
        String ifPartition = "0";
        Integer partitionNum = 1;
        //用来生成key
        String role = "我就是要看到";
        Map<String,Object> res =kafkaProducerServer.sndMesForTemplate
                (topic, value, ifPartition, partitionNum, role);

        System.out.println("测试结果如下：===============");
        String message = (String)res.get("message");
        String code = (String)res.get("code");

        System.out.println("code:"+code);
        System.out.println("message:"+message);

        return "OK";
    }
}
