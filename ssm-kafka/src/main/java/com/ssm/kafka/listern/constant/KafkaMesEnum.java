package com.ssm.kafka.listern.constant;

/**
 * kafka返回信息解析枚举类
 */
public enum KafkaMesEnum {

    // 成功码及成功信息
	SUCCESS_CODE ("00000")
	,SUCCESS_MES ("成功")
	
	,KAFKA_SEND_ERROR_CODE ("30001")
    ,KAFKA_SEND_ERROR_MES ("发送消息超时,联系相关技术人员")

    ,KAFKA_NO_RESULT_CODE ("30002")
    ,KAFKA_NO_RESULT_MES("未查询到返回结果,联系相关技术人员")

    ,KAFKA_NO_OFFSET_CODE ("30003")
    ,KAFKA_NO_OFFSET_MES ("未查到返回数据的offset,联系相关技术人员");


    // 成员变量
    private String value;

    /**
     * 枚举类构造方法必须是私有
     * @param value
     */
    private KafkaMesEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
	
}
