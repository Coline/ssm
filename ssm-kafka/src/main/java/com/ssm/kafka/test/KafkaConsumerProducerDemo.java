package com.ssm.kafka.test;

public class KafkaConsumerProducerDemo {
    public static void main(String[] args) {
        boolean isAsync = args.length == 0 || !args[0].trim().equalsIgnoreCase("sync");
//        Producer producerThread = new Producer("ssm-java_te", isAsync);
//        producerThread.start();

        ConsumerThread consumerThread = new ConsumerThread("ssm-java_te");
        consumerThread.start();

    }
}
