package com.ssm.dubbo.demo.service.inf;

public interface HelloServiceInf {
    String sayHello(String name);
}
