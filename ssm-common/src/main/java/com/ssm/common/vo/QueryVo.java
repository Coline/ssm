package com.ssm.vo;

/**
 * 
 * <p>Title: QueryVo</p>
 * <p>Description:分页查询 </p>
 * @author	yuze
 */
public class QueryVo {
	
	//页码
	private String page;
	//数量
	private String limit;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}
}
