package com.ssm.common.model.external.wechart.cloth;

import com.ssm.po.cloth.label.ClothLabelPo;
import com.ssm.po.cloth.label.LabelPicturePo;

import java.util.List;

public class QueryLabelModel {
    private ClothLabelPo clothLabelPo;
    private List<LabelPicturePo> labelPicturePos;

    public ClothLabelPo getClothLabelPo() {
        return clothLabelPo;
    }

    public void setClothLabelPo(ClothLabelPo clothLabelPo) {
        this.clothLabelPo = clothLabelPo;
    }

    public List<LabelPicturePo> getLabelPicturePos() {
        return labelPicturePos;
    }

    public void setLabelPicturePos(List<LabelPicturePo> labelPicturePos) {
        this.labelPicturePos = labelPicturePos;
    }
}
