package com.ssm.common.model.external.wechart.cloth;

import com.ssm.po.cloth.label.ClothLabelPo;

import java.util.List;

public class QueryLabelsModel {
    private List<ClothLabelPo> data;
    private Integer count;

    public List<ClothLabelPo> getData() {
        return data;
    }

    public void setData(List<ClothLabelPo> data) {
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
