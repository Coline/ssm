package com.ssm.common.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

public class PropertyManager {
    private static final Map<String, PropertyManager> map = new HashMap();
    private static Object managerLock = new Object();
    private static String propPath = "/";
    private static String defaultName = "application";
    private Properties properties = null;
    private Object propertiesLock = new Object();
    private String resourceURI;
    private static String sAppServer = null;
    private static final int MAJOR_VERSION = 1;
    private static final int MINOR_VERSION = 2;
    private static final int REVISION_VERSION = 4;

    public static String getProperty(String name, String[] propertyFileName) {
        return getPropertyObject(propertyFileName).getProp(name);
    }

    private static PropertyManager getPropertyObject(String[] propertyName) {
        String name;
        if ((propertyName == null) || (propertyName.length == 0) || (propertyName[0] == null) || (propertyName[0].trim().equals(""))) {
            name = defaultName;
        } else name = propertyName[0];
        PropertyManager manager = map.get(name);
        if (manager == null) {
            String fullPath = propPath + name + ".properties";
            synchronized (managerLock) {
                if (manager == null) {
                    manager = new PropertyManager(fullPath);
                    map.put(name, manager);
                }
            }
        }
        return manager;
    }

    public static void setProperty(String name, String value, String[] propertyName) {
        getPropertyObject(propertyName).setProp(name, value);
    }

    public static void deleteProperty(String name, String[] propertyName) {
        getPropertyObject(propertyName).deleteProp(name);
    }

    public static Enumeration propertyNames(String[] propertyName) {
        return getPropertyObject(propertyName).propNames();
    }

    public static boolean propertyFileIsReadable(String[] propertyName) {
        return getPropertyObject(propertyName).propFileIsReadable();
    }

    public static boolean propertyFileIsWritable(String[] propertyName) {
        return getPropertyObject(propertyName).propFileIsWritable();
    }

    public static boolean propertyFileExists(String[] propertyName) {
        return getPropertyObject(propertyName).propFileExists();
    }

    private PropertyManager(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    protected String getProp(String name) {
        if (this.properties == null) {
            synchronized (this.propertiesLock) {
                if (this.properties == null) {
                    loadProps();
                }
            }
        }

        String property = this.properties.getProperty(name);
        if (property == null) {
            return null;
        }
        return property.trim();
    }

    protected void setProp(String name, String value) {
        synchronized (this.propertiesLock) {
            if (this.properties == null)
                loadProps();

            this.properties.setProperty(name, value);
            saveProps();
        }
    }

    protected void deleteProp(String name) {
        synchronized (this.propertiesLock) {
            if (this.properties == null)
                loadProps();

            this.properties.remove(name);
            saveProps();
        }
    }

    protected Enumeration propNames() {
        if (this.properties == null)
            synchronized (this.propertiesLock) {
                if (this.properties == null)
                    loadProps();
            }


        return this.properties.propertyNames();
    }

    private void loadProps() {
        this.properties = new Properties();
        InputStream in = null;
        try {
            in = super.getClass().getResourceAsStream(this.resourceURI);
            this.properties.load(in);
        } catch (Exception e) {
            System.err.println("Error reading Application properties in PropertyManager.loadProps() " + e);

            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (Exception e) {
            }
        }
    }

    private void saveProps() {
        String path = "";
        OutputStream out = null;
        try {
            path = this.properties.getProperty("path").trim();
            out = new FileOutputStream(path);
            this.properties.store(out, "application.properties -- " + new Date());
        } catch (Exception ioe) {
            System.err.println("There was an error writing application.properties to " + path + ". " + "Ensure that the path exists and that the Application process has permission " + "to write to it -- " + ioe);

            ioe.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (Exception e) {
            }
        }
    }

    public boolean propFileIsReadable() {
        InputStream in;
        try {
            in = super.getClass().getResourceAsStream(this.resourceURI);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public boolean propFileExists() {
        String path = getProp("path");
        if (path == null)
            return false;

        File file = new File(path);

        return (file.isFile());
    }

    public boolean propFileIsWritable() {
        String path = getProp("path");
        File file = new File(path);
        if (file.isFile()) {
            return (file.canWrite());
        }

        return false;
    }

    public static String getAppVersion() {
        return "1.2.4";
    }

    public static int getAppVersionMajor() {
        return 1;
    }

    public static int getAppVersionMinor() {
        return 2;
    }

    public static int getAppVersionRevision() {
        return 4;
    }

    public static void main(String[] args) {
        System.out.println(getProperty("label_picture_base_path", null));
    }
}
