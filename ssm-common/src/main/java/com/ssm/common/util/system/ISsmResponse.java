package com.ssm.common.util.system;

import java.util.HashMap;

public class ISsmResponse extends HashMap<String,Object> {

    /**
     * 设置消息
     * @param ssmResponse
     * @param message
     */
    public void setMessage(ISsmResponse ssmResponse,String message){
        ssmResponse.put("message",message);
    }

    /**
     * 设置数据
     * @param ssmResponse
     * @param Data
     */
    public void setDate(ISsmResponse ssmResponse,Object Data){
        ssmResponse.put("Data",Data);
    }
}
