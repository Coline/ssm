package com.ssm.common.util.layui;

import java.util.HashMap;
import java.util.List;

public class LayuiUnit extends HashMap<String, Object> {
    public static LayuiUnit data(Integer count, List<?> data){
        LayuiUnit layuiUnit = new LayuiUnit();
        layuiUnit.put("code", 0);
        layuiUnit.put("msg", "");
        layuiUnit.put("count", count);
        layuiUnit.put("data", data);
        return layuiUnit;
    }
}
