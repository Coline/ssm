package com.ssm.common.util.file;

import com.ssm.common.util.DateUtil;
import com.ssm.common.util.PropertyManager;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.UUID;

public class UploadFile {
    /**
     * 工具类：上传文件：改名字
     */
    public static String saveFileAndChangeName(MultipartFile file) {
        // 判断文件是否为空
        if (!file.isEmpty()) {
            try {
                String dataStr = DateUtil.getDateTime_I();
                String separator = File.separator;
                String basePath = PropertyManager.getProperty("label_picture_base_path", null);
                String baseDirectory = PropertyManager.getProperty("label_picture_directory", null);
                String directory = basePath+ baseDirectory + separator+ dataStr + separator;
                String newPicName = "";
                if (file.getSize() != 0) {
                    String originalFileNameLeft = file.getOriginalFilename();
                    // 新的图片名称
                    newPicName = UUID.randomUUID()
                            + originalFileNameLeft
                            .substring(originalFileNameLeft
                                    .lastIndexOf("."));
                    // 新图片，写入磁盘
                    File f = new File(directory, newPicName);
                    if (!f.exists()) {
                        // 先创建文件所在的目录
                        f.getParentFile().mkdirs();
                    }
                    file.transferTo(f);
                }
                return separator+baseDirectory+separator+dataStr + separator + newPicName;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    /**
     * 工具类：上传文件：不改名字
     */
    public static String saveFileWithoutChangeName(MultipartFile file) {
        // 判断文件是否为空
        if (!file.isEmpty()) {
            try {
                String dataStr = DateUtil.getDateTime_I();
                String separator = File.separator;
                String picPath2 = System.getProperty("user.dir").replace("bin","webapps")+ File.separator;
                        String directory = picPath2 + "labelPictureFile" + separator+ dataStr + separator;
                String newPicName = "";
                if (file.getSize() != 0) {
                    String originalFileNameLeft = file.getOriginalFilename();
                    // 新的图片名称
                    int index = originalFileNameLeft.lastIndexOf(".");
                    newPicName = originalFileNameLeft.substring(0, index)
                            + DateUtil.getDateTime_IV()
                            + originalFileNameLeft.substring(index);
                    // 新图片，写入磁盘
                    File f = new File(directory, newPicName);
                    if (!f.exists()) {
                        // 先创建文件所在的目录
                        f.getParentFile().mkdirs();
                    } else {
                        f.delete();
                    }

                    file.transferTo(f);
                }
                return dataStr + separator + newPicName;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }
}
